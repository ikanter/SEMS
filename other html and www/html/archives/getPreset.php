<?php
  $servername = "localhost";
  $username = "root";
  $password = "shgl530";
  $dbname = "test";

  $deviceID = "test1234";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }

  $sql = "SELECT s.preset, s.price, s.refrigeratorWeight, s.kitchenWeight, s.washerdryerWeight, s.tvWeight, s.computerWeight, s.lampWeight, s.otherWeight FROM deviceID d, settings s WHERE d.deviceID = '" . $deviceID . "' AND d.id = s.id";

  $result = mysqli_query($conn, $sql);

  $preset = -1;
  $price = -1;
  $refrigerator = -1;
  $kitchen = -1;
  $washerdyer = -1;
  $tv = -1;
  $computer = -1;
  $lamp = -1;
  $other = -1;

  if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_array($result)) {
      $preset = $row['preset'];
      $price = $row['price'];
      $refrigerator = $row['refrigeratorWeight'];
      $kitchen = $row['kitchenWeight'];
      $washerdryer = $row['washerdryerWeight'];
      $tv = $row['tvWeight'];
      $computer = $row['computerWeight'];
      $lamp = $row['lampWeight'];
      $other = $row['otherWeight'];
    }
  }

  mysqli_close($conn);

  echo "<p id='presetsSection'>Presets</p>\n";
  echo "<input type='radio' name='choice' value='econ' onchange='presetchange_economy()' ";
  if ($preset == "1")
    echo "checked ";
  echo "/> Economy\n";

  echo "<input type='radio' name='choice' value='green' onchange='presetchange_green()' ";
  if ($preset == 2)
    echo "checked ";
  echo "/> Green\n";

  echo "<input type='radio' name='choice' value='mod' onchange='presetchange_moderate()' ";
  if ($preset == 3)
    echo "checked ";
  echo "/> Moderate\n";

  echo "<input type='radio' name='choice' value='high' onchange='presetchange_high()' ";
  if ($preset == 4)
    echo "checked ";
  echo "/> High\n";

  echo "<input type='radio' name='choice' value='cust' id='custom' onchange='presetchange_custom()' ";
  if ($preset == 0)
    echo "checked ";
  echo "/> Custom\n";
  echo "<br/>";
  echo "<br/>";

  echo "<p>Price Threshold: <input id='priceThresh' type='text' name='threshold' style='width:4em;text-align:right' maxlength='5' onkeypress='return isNumber(event)' ";
  if ($price >= 0)
    echo "value='" . round($price) . "' ";
  echo "/> cents/kWh</p>\n";

  echo "<table id='appl_table'>\n";
  echo "<tr><td>Appliances</td>\n";
  echo "<td width='25px' align='center'>1</td>\n";
  echo "<td width='25px' align='center'>2</td>\n";
  echo "<td width='25px' align='center'>3</td>\n";
  echo "<td width='25px' align='center'>4</td>\n";
  echo "<td width='25px' align='center'>5</td></tr>\n";

  echo "<tr><td>Refrigerator</td>\n";
  echo "<td align='center'><input type='radio' id='refrigerator1' name='fridge_weight' value='1' onchange='weightchange()' ";
  if ($refrigerator == 1)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='refrigerator2' name='fridge_weight' value='2' onchange='weightchange()' ";
  if ($refrigerator == 2)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='refrigerator3' name='fridge_weight' value='3' onchange='weightchange()' ";
  if ($refrigerator == 3)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='refrigerator4' name='fridge_weight' value='4' onchange='weightchange()' ";
  if ($refrigerator == 4)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='refrigerator5' name='fridge_weight' value='5' onchange='weightchange()' ";
  if ($refrigerator == 5)
    echo "checked ";
  echo "/></td></tr>\n";

  echo "<tr><td>Kitchen</td>\n";
  echo "<td align='center'><input type='radio' id='kitchen1' name='kitch_weight' value='1' onchange='weightchange()' ";
  if ($kitchen == 1)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='kitchen2' name='kitch_weight' value='2' onchange='weightchange()' ";
  if ($kitchen == 2)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='kitchen3' name='kitch_weight' value='3' onchange='weightchange()' ";
  if ($kitchen == 3)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='kitchen4' name='kitch_weight' value='4' onchange='weightchange()' ";
  if ($kitchen == 4)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='kitchen5' name='kitch_weight' value='5' onchange='weightchange()' ";
  if ($kitchen == 5)
    echo "checked ";
  echo "/></td></tr>\n";

  echo "<tr><td>Washer/Dryer</td>\n";
  echo "<td align='center'><input type='radio' id='washer1' name='wash_weight' value='1' onchange='weightchange()' ";
  if ($washerdryer == 1)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='washer2' name='wash_weight' value='2' onchange='weightchange()' ";
  if ($washerdryer == 2)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='washer3' name='wash_weight' value='3' onchange='weightchange()' ";
  if ($washerdryer == 3)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='washer4' name='wash_weight' value='4' onchange='weightchange()' ";
  if ($washerdryer == 4)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='washer5' name='wash_weight' value='5' onchange='weightchange()' ";
  if ($washerdryer == 5)
    echo "checked ";
  echo "/></td></tr>\n";

  echo "<tr><td>TV</td>\n";
  echo "<td align='center'><input type='radio' id='tv1' name='tv_weight' value='1' onchange='weightchange()' ";
  if ($tv == 1)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='tv2' name='tv_weight' value='2' onchange='weightchange()' ";
  if ($tv == 2)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='tv3' name='tv_weight' value='3' onchange='weightchange()' ";
  if ($tv == 3)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='tv4' name='tv_weight' value='4' onchange='weightchange()' ";
  if ($tv == 4)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='tv5' name='tv_weight' value='5' onchange='weightchange()' ";
  if ($tv == 5)
    echo "checked ";
  echo "/></td></tr>\n";

  echo "<tr><td>Computer</td>\n";
  echo "<td align='center'><input type='radio' id='computer1' name='pc_weight' value='1' onchange='weightchange()' ";
  if ($computer == 1)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='computer2' name='pc_weight' value='2' onchange='weightchange()' ";
  if ($computer == 2)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='computer3' name='pc_weight' value='3' onchange='weightchange()' ";
  if ($computer == 3)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='computer4' name='pc_weight' value='4' onchange='weightchange()' ";
  if ($computer == 4)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='computer5' name='pc_weight' value='5' onchange='weightchange()' ";
  if ($computer == 5)
    echo "checked ";
  echo "/></td></tr>\n";

  echo "<tr><td>Lamp</td>\n";
  echo "<td align='center'><input type='radio' id='lamp1' name='lamp_weight' value='1' onchange='weightchange()' ";
  if ($lamp == 1)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='lamp2' name='lamp_weight' value='2' onchange='weightchange()' ";
  if ($lamp == 2)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='lamp3' name='lamp_weight' value='3' onchange='weightchange()' ";
  if ($lamp == 3)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='lamp4' name='lamp_weight' value='4' onchange='weightchange()' ";
  if ($lamp == 4)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='lamp5' name='lamp_weight' value='5' onchange='weightchange()' ";
  if ($lamp == 5)
    echo "checked ";
  echo "/></td></tr>\n";

  echo "<tr><td>Other</td>\n";
  echo "<td align='center'><input type='radio' id='other1' name='other_weight' value='1' onchange='weightchange()' ";
  if ($other == 1)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='other2' name='other_weight' value='2' onchange='weightchange()' ";
  if ($other == 2)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='other3' name='other_weight' value='3' onchange='weightchange()' ";
  if ($other == 3)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='other4' name='other_weight' value='4' onchange='weightchange()' ";
  if ($other == 4)
    echo "checked ";
  echo "/></td>\n";
  echo "<td align='center'><input type='radio' id='other5' name='other_weight' value='5' onchange='weightchange()' ";
  if ($other == 5)
    echo "checked ";
  echo "/></td></tr>\n";

  echo "</table>\n";
  echo "1: always off; 2: threshold minus 3; 3: at threshold; 4: threshold plus 3; 5: always on\n";
  echo "<br/>";
  echo "<button id='presetSubmit' type='button' class='btn btn-warning' style='color:black' onclick='submitonclick()'>Submit</button>\n";
?>
