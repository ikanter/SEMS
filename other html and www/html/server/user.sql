DROP TABLE IF EXISTS userSettings;
DROP TABLE IF EXISTS offPeriods;

CREATE TABLE userSettings(
	applianceID		int		NOT NULL AUTO_INCREMENT,
	name			varchar (30),
	power			int		(4),
	onOffPolicy		int		(1),
	threshold		float	(9),
	icon			varchar	(30),
	state			int		(1),
	primary key (applianceID)
);

CREATE TABLE offPeriods(
	scheduleID		int		NOT NULL AUTO_INCREMENT,
	applianceID		int		(11),
	day				varchar (30),
	beginTime		char	(5),
	endTime			char	(5),
	primary key (scheduleID)
);

/*
CREATE TABLE onOffTime(
	Date			TIMESTAMP NOT NULL DEFAULT NOW(),
	applianceID		int		(11),
	state			int 	(1)
);	
*/

CREATE TABLE LMP(
	dateID			int		NOT NULL AUTO_INCREMENT,
	Date			char	(16),
	LMP				char	(10),
	primary key (dateID)
);

/*
CREATE TABLE Dates( 
	dateID			int		NOT NULL AUTO_INCREMENT,
	Date			char	(16),
	primary key (dateID)
);
*/

CREATE TABLE State(
	dateID			int		(11),
	applianceID		int		(11),
	state			int		(1),
	policy			int		(1)
);




INSERT INTO userSettings 
	(name, power, onOffPolicy, threshold, icon) 
	VALUES ('Lamp', 60, 0, 389, 'icon.png');

INSERT INTO offPeriods 
	(applianceID, day, beginTime, endTime) 
	VALUES (1, 'sun,mon,tue', '11:30', '12:30');


INSERT INTO offPeriods
    (applianceID, day, beginTime, endTime)
	VALUES (1, 'tue,wed,fri', '13:30', '14:30');





SELECT * FROM userSettings, offPeriods
	WHERE userSettings.applianceID = offPeriods.applianceID; 
