<?php
	//ini_set('memory_limit', '1024M');
	$servername = "localhost";
	$username = "root";
	$password = "mk530";
	$dbname = "test";

	$conn = mysqli_connect($servername, $username, $password, $dbname);
	if(!$conn){
		die("Connection failed: " . mysqli_connect_error());
	}

	// Query the price information from the database
	$selectQuery = "SELECT date, LMP, GROUP_CONCAT(State.applianceID SEPARATOR ',') AS applianceID,
								GROUP_CONCAT(name SEPARATOR ',') AS name,
								GROUP_CONCAT(State.state SEPARATOR ',') AS state,
								GROUP_CONCAT(power SEPARATOR ',') AS power ,
								GROUP_CONCAT(policy SEPARATOR ',') AS policy
								FROM State, LMP, userSettings 
								WHERE LMP.dateID = State.dateID 
									and userSettings.applianceID = State.applianceID 
								GROUP BY State.dateID;";
	
	$queryResult = mysqli_query($conn, $selectQuery);
	
	$state = array();
	$power = array();
	$applianceID = array();
			
	echo "energyData = [";
	// Output the price information to a script
	while($row = mysqli_fetch_array($queryResult)){	
		$applianceID = explode(',', $row["applianceID"]);
		$state  = explode(',', $row["state"]);
		$power  = explode(',', $row["power"]);
		$policy = explode(',', $row["policy"]);
		$name   = explode(',', $row["name"]);
		$LMP = $row["LMP"];	

		for($i = 0; $i < count($applianceID); $i++){	
			$tuple = array("datestr" => $row["date"], 
							"applianceID" => $applianceID[$i],	
							"LMP" => $LMP,
							"name" => $name[$i],
							"state" => $state[$i],
							"power" => $power[$i],
							"policy" => $policy[$i]	
							);
											
			echo json_encode($tuple).",";
		}	
	}
	echo "];";	

	mysqli_close($conn);
?>
