<?php

$servername = "localhost";
$username = "root";
$password = "mk530";
$dbname = "test";

//$q = $_GET['q'];
$deviceID = "test1234";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if(!$conn){
	die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT p.outletSet, p.beginHour, p.beginMinute, p.endHour, p.endMinute, p.monday, 
		p.tuesday, p.wednesday, p.thursday, p.friday, p.saturday, p.sunday 
	FROM deviceID AS d, offPeriods AS p 
	WHERE d.deviceID = '" . $deviceID . "' AND d.id = p.id";
	
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0){
  $counter = 0;
  while($row = mysqli_fetch_array($result)) {
    $outletSet = $row['outletSet'];
    $beginHour = $row['beginHour'];
    $endHour = $row['endHour'];
    if ($beginHour > 12) {
      $beginHour = +$beginHour - 12;
      $ampm1 = "pm";
    } else if ($beginHour == 12) {
      $ampm1 = "pm";
    } else if ($beginHour < 12 && $beginHour > 0) {
      $ampm1 = "am";
    } else if ($beginhour == 0) {
      $beginHour = +$beginHour + 12;
      $ampm1 = "am";
    }
    if ($endHour > 12) {
      $endHour = +$endHour - 12;
      $ampm2 = "pm";
    } else if ($endHour == 12) {
      $ampm2 = "pm";
    } else if ($endHour < 12 && $endHour > 0) {
      $ampm2 = "am";
    } else if ($endHour == 0) {
      $endHour = +$endHour + 12;
      $ampm2 = "am";
    }
    $beginMin = sprintf("%02d", $row['beginMinute']);
    $endMin = sprintf("%02d", $row['endMinute']);
    $periodName = $outletSet . " " .
                  $beginHour . ":" . $beginMin . $ampm1 . "-" .
                  $endHour . ":" . $endMin . $ampm2 . " " .
                  $row['monday'] . " " .
                  $row['tuesday'] . " " .
                  $row['wednesday'] . " " .
                  $row['thursday'] . " " .
                  $row['friday'] . " " .
                  $row['saturday'] . " " .
                  $row['sunday'];
    echo "<option value ='p" . $counter . "' id='p" . $counter . "'>" . $periodName . "</option>";
    $counter++;
  }
}


mysqli_close($conn);
?>
