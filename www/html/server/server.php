<script>

/* Some tips
//Larry: For examples in using the query system in node.js, navigate to the file
//named test.js in the /var/www/html/server/archives directory.

//Larry: Much of server.js has been commented out as a result of changing the
//api from which we pull our pricing data and also testing blocks which may be useful
//in the future.
*/

var http	= require("http");
var request = require("requestretry");
var mysql	= require('mysql');
var yql = require('yql-node').formatAsJSON();



//Credentials for MySQL Database Connection
var pool = mysql.createPool({
	host     : 'localhost',
	user     : 'root',
	password : 'root',
	database : 'test'
});



//pool.end();


/* //Larry: Some prepared queries are no longer needed as a result of changing apis
//Larry: Prepared SQL queries commented out
//var createLMP	= "CREATE TABLE LMP(Date char(16), LMP char(10))";
//var deleteLMP	= "DROP TABLE IF EXISTS LMP";
//var insertLMP	= "INSERT INTO LMP (Date, LMP) VALUES(?,?)";
*/					
var insertDate  = "INSERT INTO Dates (Date) VALUES(?)";
var insertState = "INSERT INTO State (dateID, applianceID, state, policy) VALUES(?,?,?,?)";		
var selectAppliance = "SELECT applianceID, state, onOffPolicy FROM userSettings";
var selectPeriods	= "SELECT userSettings.applianceID, day, beginTime, endTime, onOffPolicy, threshold, frequency FROM userSettings LEFT JOIN offPeriods ON offPeriods.applianceID = userSettings.applianceID ORDER BY applianceID";
var updateState		= "UPDATE userSettings SET state = ? WHERE applianceID = ?";
var selectPolicy	= "SELECT onOffPolicy, threshold FROM userSettings WHERE applianceID = ?";
var selectFrequency = "SELECT Frequency FROM Frequency";
var selectFreqThreshold = "SELECT applianceID, Frequency FROM userSettings";
//pool.query(deleteLMP);
//pool.query(createLMP);

//Larry: Authentication no longer needed since we changed apis
//Authentication for ISO-NE API
//var username = "smarthomeenergymanagementsystm@gmail.com";
//var password = "SHEMSshgl530";

/* //Larry: options for the old api commented out
   //Set options to retrive info from ISO-NE API
var options = {
	"rejectUnauthorized": false,
	url: "https://webservices.iso-ne.com/api/v1.1/fiveminutelmp/current/location/4004.json",
	method: 'GET',
	maxAttempts: 5,
	retryDelay: 5000,
	retryStrategy: request.RetryStrategies.HTTPOrNetworkError,
	auth: {
		user: username,
		password: password
	}
};
*/

var id;
var pastDate = "0";

function getDateFormatted(date){ //Larry: This returns a very specific format which is needed for the nyiso api url
	var thisMonth = date.getMonth()+1;
	
	if (thisMonth<10){
		
		thisMonth = '0' +  thisMonth;
		
	}
	
	var dateFormatted = date.getFullYear() + '' + thisMonth;
	
	if (date.getDate()<10){
		
		dateFormatted = dateFormatted + '0' + date.getDate();
		
	}else{
		
		dateFormatted = dateFormatted + '' + date.getDate();
	}
	
	//Larry: returns the date in the form: '20170820' if the date were August 20th 2017
	
	return dateFormatted;
	
}
									 
function changeState(newState, id){ //Larry: This function is merely an alternate way of querying the db
									 //		  and is unused atm
	
	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		//Larry: The following is akin to a prepared query 
		
		var sql = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
		
		var inserts = ['userSettings', 'state', newState,  'applianceID', id];
		
		sql = mysql.format(sql, inserts);
		
		connection.query(sql);
		
		connection.release();
			
	});
		
}

function updatePrice(newPrice){ //Larry: This function is merely an alternate way of querying the db
									 //		  and is unused atm
	
	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		//Larry: The following is akin to a prepared query 
		
		var sql = "UPDATE ?? SET ?? = ?";
		
		var inserts = ['NYPrices', 'currentPrice', newPrice];
		
		sql = mysql.format(sql, inserts);
		
		connection.query(sql);
		
		connection.release();
			
	});
		
}

function updateDate(newDate){ //Larry: This function is merely an alternate way of querying the db
									 //		  and is unused atm
	
	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		//Larry: The following is akin to a prepared query 
		
		var sql = "UPDATE ?? SET ?? = ?";
		
		var inserts = ['NYPrices', 'Date', newDate];
		
		sql = mysql.format(sql, inserts);
		
		connection.query(sql);
		
		connection.release();
			
	});
		
}
var d;
function getArchivedDate(){ //Larry: This function is merely an alternate way of querying the db
									 //		  and is unused atm
	
	
	
	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		//Larry: The following is akin to a prepared query 
		
		var sql = "SELECT ?? AS date FROM ??";
		
		var inserts = ['Date', 'NYPrices'];
		
		sql = mysql.format(sql, inserts);
		
		connection.query(sql, function(err, rows, fields){
			
			if (err) throw err;
			
			d = rows[0].date;
						
			console.log('Inside Connection1: ' + d)
			
			
			if (d){
				
				console.log('Inside Connection2: ' + d)
				
				connection.release();
				
				console.log('Inside Connection3: ' + d)
			}
			console.log('Inside Connection4: ' + d)
		});
				console.log('Outside Connection: ' + d);
	});
			console.log('Outside Pool: ' + d)
}

//Larry: Retrive info from nyiso API and directly compares values from database

var jj = 0;
var currPrice;
var NyObject = {};
function func(){ //Larry: changed the api from which pricing data is generated
				  //		Applies to entire function
				  //		Polls the nyiso site every minute(see interval at bottom)

	//Larry: This will always provide most current date
	
	var globalDate = new Date();
	
	//Larry: As the name implies, this will format in a manner suitable for the url query
	
	var dFormatted = getDateFormatted(globalDate);
	
	date = new Date();		

	
	var thisMonth = date.getMonth()+1;
	
	if (thisMonth<10){
		
		thisMonth = '0' +  thisMonth;
		
	}
	
	var thisDay;
	if (date.getDate()<10){
		
		thisDay = '0' + date.getDate();
		
	}else{
		
		thisDay = date.getDate();
		
	}
	
	var dateFormatted = thisMonth + '/' + thisDay + '/' + date.getFullYear();
	var thisSecond = date.getSeconds();
	var thisMinute = date.getMinutes();
	var thisHour = date.getHours();
	
	if (thisSecond < 10){
		
		thisSecond = '00';
		//Larry: The second will not increment so that the minute can match up when in range
	}else thisSecond = '00';
	
	if (thisMinute < 10){
		
		thisMinute = '0' + thisMinute;
		
	}else thisMinute = thisMinute;
	
	if (thisHour < 10){
		
		thisHour = '0' + thisHour;
		
	}else thisHour = thisHour;
	
	var timeFormatted = thisHour + ':' + thisMinute + ':' + thisSecond;
		
	/*  //Larry: Url build-up explanation
	//The first variable is the queried api, which requires the formatted date to be current for the commented out url
	//The following variables filter, refine and optimize the results of the query
	//so that instead of receiving 1000's of data-points in no particular order from the api,
	//everytime the interval is hit, only a limited number of the most current results are returned, in
	//a specific order and from a specific area
	
	//If desired, extensive documentation can be found online by googling "YQL optimization"
	*/
//	var nyMIS = "http://mis.nyiso.com/public/csv/damlbmp/" + dFormatted + "damlbmp_zone.csv //Where the date formatted would be one day ahead for a future projected lbmp
//	var nyMIS = "http://mis.nyiso.com/public/csv/realtime/" + dFormatted + "realtime_zone.csv"; //This url differs from the url below in that this will return many more values from the current day
	var nyMIS = "http://mis.nyiso.com/public/realtime/realtime_zone_lbmp.csv"; //This url should return one, most recently updated value '''Testing'''
	var filter = " and col1 = 'N.Y.C.'";
	var sort = "sort(field='col0',descending='false')";
	var range = "tail(count=3)";
	var nyUrl = "select * from csv where url = '" + nyMIS + "'" + filter + "|" + sort + "|" + range + "";
	
/*	//Larry: This is an alternate way of extraction from the above nyiso api
	request({
		
		url: nyUrl,
		json: true,
		maxAttempts: 10,
		retryDelay: 10000,
		retryStrategy: request.RetryStrategies.HTTPOrNetworkError
	}, function(err, response, body){
		
		if (err) throw err;
		
		if(response) {
			
			console.log("The number of request attempts: " + response.attempts);
			console.log(response);
			console.log(body);
			
		}
		
	});
*/ 
	
/* //Larry: Old api call commented out

	request(options, function(err, res, body){ //body = JSON object returned by API call
		if(err){
			console.log(err);
			return;
		}
	
		var jsonObject = JSON.parse(body); //Save JSON object
		var day = jsonObject.FiveMinLmp[0].BeginDate.substring(0,16); //Save the Date
		lmp = jsonObject.FiveMinLmp[0].LmpTotal; //Save the LMP	
		
		if(pastDate == day){
			return;
		}
		else{
			pastDate = day;
		}

		//Connect to DB
		pool.getConnection(function(err, connection){
			if(err){
				console.log('Error connecting to DB');
				console.log(err);
				return;
			}
			console.log('Connection established');
					
			//Query the DB 
			connection.query(insertLMP,[day, lmp],function(err, rows, fields){
//				connection.release();
				if(!err){
					console.log('Date: ', day);
					console.log('LMP:  ', lmp);	
					id = rows.insertId;
					console.log(id);
					
					connection.query(selectAppliance, function(err, rows, fields){	
						for(var i = 0; i < rows.length; i++){
							connection.query(insertState, [id, rows[i].applianceID, rows[i].state, rows[i].onOffPolicy],function(err, rows, fields){
//								connection.release();
								if(!err){
									console.log('Inserted stuff into DB');
								}
								else{
									console.log('Error while performing inserState Query');
								}
							});
						}
					});	
					connection.release();
				//	return;
				}
				else{
					console.log('Error while performing insertLMP Query.');	
				}
			});	
		});
	});
	 
*/ //Old api call commented out
	
	yql.execute(nyUrl, function(error, response){//Larry: Added yql functionality
												  //This command is made possible by the above package, 'yql-node'
												  //which utilizes yahoo's query service to make the necassary query
												  //a great deal simpler to use in node.js
			
			if (Date.parse(response.query.results.row.col0) <= Date.parse(NyObject.col0)){//if true, don't update currPrice
				
				jj++;
				console.log(jj + ': ');				
				console.log('\nThe queried date is not more recent than previously archived:\n');
				console.log('\t\t\t\t\t\t   Queried Date  : ' + response.query.results.row.col0);
				console.log('\t\t\t\t\t\t   Archived Date : ' + NyObject.col0);			
				console.log('\t\t\t\t\t\t   Archived Price: $' + NyObject.col3 + '\n');			

			} else{//update currPrice
				
				jj++;
				console.log(jj + ': ');		
				console.log('\nThe queried date is more recent than archived:\n');
				console.log('\t\t\t\t\t\t   Queried Date  : ' + response.query.results.row.col0);
				console.log('\t\t\t\t\t\t   Archived Date : ' + NyObject.col0);
				if (NyObject.col3){//if it's a number
					console.log('\t\t\t\t\t\t   Archived Price: $' + NyObject.col3);
				} else{//if not
					console.log('\t\t\t\t\t\t   Archived Price: ' + NyObject.col3);
				}
				console.log('\t\t\t\t\t\t   New Price     : $' + response.query.results.row.col3);
				NyObject = response.query.results.row;
				currPrice = Number(NyObject.col3);
				updatePrice(currPrice);//Updates db
				updateDate(Date.parse(NyObject.col0));//Updatesdb
				
			}//else
			
			
/*		
		for(i = 0; i < response.query.results.row.length; i++){ //Larry: Loops through all returned query rows
			
			var dateString = response.query.results.row[i].col0.substr(0, 10);
			var timeString = response.query.results.row[i].col0.substr(11);

					
			if (dateFormatted === dateString && timeFormatted === timeString){ //Larry: Only if the queried date and time match the current date and time
																			   //exactly does this if evaluate true
				
				console.log('Time Formatted: ' + timeFormatted);
				console.log("Query Date: " + dateString);
				console.log("Query Time: " + timeString);
				console.log("Current Date: " + dateFormatted);						//Larry: Aesthetics										  
				console.log(' ');
				console.log(' ');
				console.log('			yql Response: ');//Larry: The following logs are more to show the structure o the returned rows than anything else
				console.log('			Time Stamp: ' + response.query.results.row[i].col0);
				console.log('			City: ' + response.query.results.row[i].col1);
				console.log('			PTID: ' + response.query.results.row[i].col2);
				console.log('			LMP: ' + response.query.results.row[i].col3);
				console.log('			Margin: ' + response.query.results.row[i].col4);
				
				NyObject = response.query.results.row
				
				if(Number(NyObject[i].col3)){
					
					console.log("			Current Price changed to LMP: " + NyObject[i].col3);
					
					//Larry: currPrice is made to be accessible by checkTime() below, which is queried every 5secs
					currPrice = Number(NyObject[i].col3);
					
				} else currPrice = currPrice;
				
				console.log(' ');
				console.log(' ');
					
				
			} //if (dateFormatted === dateString && timeFormatted === timeString)

		} //for(i = 0; i < response.query.results.row.length; i++)
*/
		


/*		//Larry: If the query, nyUrl above, wasn't optimized, this would be the way to get the results sorted
		console.log('yql: ');
		var appropriateLength = response.query.results.row.length - 6;
		//var NyObject = response.query.results.row[22];
		for (i = appropriateLength; i < response.query.results.row.length; i++){
			if (response.query.results.row[i].col1 === 'N.Y.C.'){
				var NyObject = response.query.results.row[i];
				console.log(response.query.results.row.length);
				console.log('Time Stamp: ' + response.query.results.row[i].col0);
				console.log('City: ' + response.query.results.row[i].col1);
				console.log('PTID: ' + response.query.results.row[i].col2);
				console.log('LMP: ' + response.query.results.row[i].col3);
				console.log('Margin: ' + response.query.results.row[i].col4);
			}
		}
		if(Number(NyObject.col3)){
			
			console.log(NyObject.col3);
			currPrice = Number(NyObject.col3);//currPrice is made to be accessible by a following function
			//console.log(currPrice);
		}
*/
/*			//Larry: The following commented out section proves redundant as a result of the checkTime() function below
			pool.getConnection(function(err, connection) {
	
				if (err) throw err;
				
				var sql = "SELECT * FROM ??";
				
				var inserts = ['userSettings'];
				
				sql = mysql.format(sql, inserts);
			
				connection.query( sql, function(err, rows, fields) {
			
					if (err) throw err;
					
					if (rows.length == 0) {
					
						console.log('Row condition: undefined');
					
					} else {
						
						for(i = 0; i < rows.length; i++){
							
							if (rows[i].onOffPolicy == 1){
						
								if (currPrice > rows[i].threshold){
									
										changeState(0, rows[i].applianceID); console.log("state changed to 0");
									
								} else {changeState(1, rows[i].applianceID); console.log("state changed to 1");} 
									
							}else {console.log("Skipping " + rows[i].name + "because of current policy.");}//if(currPrice > rows[i].threshold)
						
						
								
	/*							console.log("There are " + rows.length + " rows.");
								
								console.log('The ID is: ', rows[i].applianceID);
					
								console.log('The name is: ', rows[i].name);
					
								console.log('The power is: ', rows[i].power);
					
								console.log('The policy is: ', rows[i].onOffPolicy);
					
								console.log('The threshold is: ', rows[i].threshold);
					
								console.log('The frequency is: ', rows[i].frequency);
					
								console.log('The state is: ', rows[i].state);
* /							}//for(i = 0; i < rows.length; i++)
						}
			
						connection.release();
					
					//else
			
				});//connection.query
		
			});//pool.connection
			
		} else console.log('NyObject.col3 is not a number');//if it's a number
*/			

	});	//yql.execute
	
}

var j=0;
var currentFrequency;
function getFrequency(){ //Larry: This function uses mach data stored in the db atm
	
	pool.getConnection(function(err, connection){
		
		if(err){
			console.log('			Error connecting to DB');
			console.log(err);
			return;
		}
		
/*		//asthetics Changes
		if(j%20==0){
			
			if(j==0){
					
				console.log(' ');
				console.log('			Connection to Frequency DB established:  First Data Point');
				console.log(' ');
				console.log(' ');
								
			} else{
				
				console.log(' ');
				console.log('			Connection to Frequency DB established:  ' + j + 'th Data Point');
				console.log(' ');
				console.log(' ');
				
			}
			
		}
*/		
		connection.query(selectFrequency, function(err, rows, fields){
			currentFrequency = rows[j].Frequency;
//Changes			console.log('			Current Frequency: ' + currentFrequency);
//Changes			console.log(' ');
			j++;
			if(j>=rows.length){j=0;}
		});
		connection.release();
	});
}

var ret, pre;

function checkTime(){//Larry: This functions checks all the things and changes all the other things accordingly,
					  //effectively controlling all the appliances by controlling the db, which is monitored by shems.bin
					  //located in /home/pi/shems directory
	var id;
	var state;
	//Connect to DB
	pool.getConnection(function(err, connection){
		if(err){
			console.log('Error connecting to DB');
			console.log(err);
			return;
		}
//		console.log('Connection established');
					
		//Query the DB 
		connection.query(selectPeriods, function(err, rows, fields){//Larry: returns rows from selectPeriods query
			for(var i = 0; i < rows.length; i++){
				
				// Check Policy
				pre = ret
				if(rows[i].onOffPolicy == 0){
					ret = 1;
				}
				
				//Larry: added and replaced else if dealing with lmp, as seen commented out below this code block
				//This else if uses currPrice, as defined in the above function, func(), after the yql query
				else if(rows[i].onOffPolicy == 1){
					if(rows[i].threshold < currPrice){	
						ret = 0;
					}
					else{
						ret = 1;
					}
				}
/*				//Larry: This else if is depreciated as a result of the additions in func() above
				else if(rows[i].onOffPolicy == 1){
					if(rows[i].threshold < lmp){	
						ret = 0;
					}
					else{
						ret = 1;
					}
				}
*/
				else if(rows[i].onOffPolicy == 2){
					ret = 0;
				}
				else if(rows[i].onOffPolicy == 3){
					if(rows[i].frequency > currentFrequency){
						ret = 0;
					}
					else{
						ret = 1;
					}
				}


				// Check Period
				if(i == 0){
					id = rows[i].applianceID;
					state = checkPeriod(rows[i], connection);	
					pre = ret;
				}

				if(id == rows[i].applianceID){
					if(state != 0){
						state = checkPeriod(rows[i], connection);		
					}
				}
				else{
					if(state == 1){
						state = pre;
					}
					connection.query(updateState, [state, id]);	
				//	console.log("2 Updated appliance: " + id + " to state: " + state);
					id = rows[i].applianceID;
					state = checkPeriod(rows[i], connection);	
				}
			}

			if(state == 1){
				state = ret;		
			}

			connection.query(updateState, [state,id]);
		//	console.log("1 Updated appliance: " + id + " to state: " + state);
		
		});
		connection.release();
	});


}


function checkPeriod(row, connection){
	var d = new Date();
	
	var day = row.day;
	if(day == null){
		return 1;
	}
	var arr = day.split(',');
	var flag = 0;

	for(var j = 0; j < arr.length; j++){

		// Change day to integer
		if(arr[j]== 'sun') arr[j] = 0;
		else if(arr[j] == 'mon') arr[j] = 1;
		else if(arr[j] == 'tue') arr[j] = 2;
		else if(arr[j] == 'wed') arr[j] = 3;
		else if(arr[j] == 'thu') arr[j] = 4;
		else if(arr[j] == 'fri') arr[j] = 5;
		else if(arr[j] == 'sat') arr[j] = 6;

		if(arr[j] == d.getDay()){
			flag = 1;
			// Check if start time is less than current time
			var start = row.beginTime.split(':');
			var end = row.endTime.split(':');
			if(start[0] < d.getHours()){
				if(end[0] > d.getHours()){
					return 0;
				}
				else if((end[0] == d.getHours()) && (end[1] >= d.getMinutes())){
					return 0;
				}
				else{
					return 1;
				}	
			}
			else if((start[0] == d.getHours()) && (start[1] <= d.getMinutes())){
				if(end[0] > d.getHours()){
					return 0;
				}
				else if((end[0] == d.getHours()) && (end[1] >= d.getMinutes())){
					return 0;
				}
				else{
					return 1;
				}
			}
			else{	
				return 1;
			}
		}
	}
	if(flag == 0){
		return 1;	
	}
}

func();
getFrequency();
//Set interval to check on/off period every 1min
//setInterval(checkTime, 60000);	//60,000ms = 1min
setInterval(checkTime, 500);		//1,000ms = 1s

//Set interval to call func every 1mins 
//setInterval(func,900000);	//900,000ms = 15min
setInterval(func,30000);	//900,000ms = 15min
//Set interval to call getFrequency every 0.5 second
setInterval(getFrequency,500);

</script>
