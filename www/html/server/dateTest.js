var http	= require("http");
var request = require("requestretry");
var mysql	= require('mysql');
var yql = require('yql-node').formatAsJSON();



function repeat(){//This function will set newDate to be exactly one day in the future
	
	var date = new Date();
	
	var newDate = new Date(date.setTime(date.getTime() + 86400000));
	
	console.log('Should be tomorrow\'s Date: ' + newDate);
	
}

setInterval(repeat, 5000);
