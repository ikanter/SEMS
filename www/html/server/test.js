//The intervals at the bottom of the page can be set to see the code in action,

//simply uncomment the intervals, and then navigate to the terminal and type 'node test.js' to run the code 

//(Similar to 'python myCode.py' to run a python code file)

var http        = require("http");
var request = require("requestretry");
var mysql       = require('mysql');
var yql = require('yql-node').formatAsJSON();



//Credentials for MySQL Database Connection
var pool = mysql.createPool({
        host     : 'localhost',
        user     : 'root',
        password : 'root',
        database : 'test'
});

var i = 1;

function testFunc0 () { //This constitutes an easy-to-follow working example of making an UPDATE query

	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		connection.query('UPDATE ?? SET ?? = ? WHERE ?? = ?', ['userSettings', 'threshold', i,  'applianceID', i]);
	
				connection.release();
			
	});
			i += 1;
	
}

function testFunc1 () { //This constitutes an alternate working example of making an UPDATE query
                        //Using this method gives full control over the query sent to the db, preventing SQL injection

	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		var sql = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
		
		var inserts = ['userSettings', 'threshold', i,  'applianceID', i];
		
		sql = mysql.format(sql, inserts);
		
		connection.query(sql);
	
		connection.release();
			
	});
			i += 1;
	
}

function testFunc2 () { //This constitutes an easy-to-follow working example of a SELECT query

	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
	
		connection.query('SELECT * FROM ?? WHERE ?? = ?', ['userSettings', 'applianceID', i], function(err, rows, fields) {
	
			if (err) throw err;
			
			if (rows.length == 0) {
			
				console.log('Row condition met: undefined');
			
			} else {
			
					
				console.log('The ID is: ', rows[0].applianceID);
	
				console.log('The name is: ', rows[0].name);
	
				console.log('The power is: ', rows[0].power);
	
				console.log('The policy is: ', rows[0].onOffPolicy);
	
				console.log('The threshold is: ', rows[0].threshold);
	
				console.log('The frequency is: ', rows[0].frequency);
	
				console.log('The state is: ', rows[0].state);
	
				connection.release();
			
			
			}
			i += 1;
	
		});
	
	});

}

function testFunc3 () { //This constitutes an easy-to-follow working example of a SELECT query
                        //Using this method gives full control over the query sent to the db, preventing SQL injection

	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		var sql = "SELECT * FROM ?? WHERE ?? = ?";
		
		var inserts = ['userSettings', 'applianceID', i];
		
		sql = mysql.format(sql, inserts);
	
		connection.query( sql, function(err, rows, fields) {
	
			if (err) throw err;
			
			if (rows.length == 0) {
			
				console.log('Row condition met: undefined');
			
			} else {
			
					
				console.log('The ID is: ', rows[0].applianceID);
	
				console.log('The name is: ', rows[0].name);
	
				console.log('The power is: ', rows[0].power);
	
				console.log('The policy is: ', rows[0].onOffPolicy);
	
				console.log('The threshold is: ', rows[0].threshold);
	
				console.log('The frequency is: ', rows[0].frequency);
	
				console.log('The state is: ', rows[0].state);
	
				connection.release();
			
			
			}
			i += 1;
	
		});
	
	});

}

function getDateFormatted(date){

	var thisMonth = date.getMonth()+1;
	
	if (thisMonth<10){
		thisMonth = '0' +  thisMonth;
	}
	
	var dateFormatted = date.getFullYear() + '' + thisMonth;
	
	if (date.getDate()<10){
		dateFormatted = dateFormatted + '0' + date.getDate();
	}else{
		dateFormatted = dateFormatted + '' + date.getDate();
	}
	
	return dateFormatted;
	
}

function changeState(newState, id){
	
	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
	
		var sql = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
		
		var inserts = ['userSettings', 'state', newState,  'applianceID', id];
		
		sql = mysql.format(sql, inserts);
		
		connection.query(sql);
		
		connection.release();
			
	});
		
}

function testFunc4 () { 
	
	var globalDate = new Date();
	var dateFormatted = getDateFormatted(globalDate);//This will always provide most current date
	var nyMIS = "http://mis.nyiso.com/public/csv/realtime/" + dateFormatted + "realtime_zone.csv";
	var nyUrl = "select * from csv where url = '" + nyMIS + "' limit 13";
	
/*	//This is an alternate way of extraction from the above nyiso api
	request({
		
		url: nyUrl,
		json: true,
		maxAttempts: 10,
		retryDelay: 10000,
		retryStrategy: request.RetryStrategies.HTTPOrNetworkError
	}, function(err, response, body){
		
		if (err) throw err;
		
		if(response) {
			
			console.log("The number of request attempts: " + response.attempts);
			console.log(response);
			console.log(body);
			
		}
		
	});
*/ 
	
	yql.execute(nyUrl, function(error, response){
		
		console.log('yql: ');
		var nyObject = response.query.results.row[10];
		if(Number(nyObject.col3)){
			
			var currPrice = Number(nyObject.col3);
			//console.log(currPrice);
			
			pool.getConnection(function(err, connection) {
	
				if (err) throw err;
				
				var sql = "SELECT * FROM ??";
				
				var inserts = ['userSettings'];
				
				sql = mysql.format(sql, inserts);
			
				connection.query( sql, function(err, rows, fields) {
			
					if (err) throw err;
					
					if (rows.length == 0) {
					
						console.log('Row condition: undefined');
					
					} else {
						
						for(i = 0; i < rows.length; i++){
							
							if (rows[i].onOffPolicy == 1){
						
								if (currPrice > rows[i].threshold){
									
									if (rows[i].state == 1){
									
										changeState(0, rows[i].applianceID);
									
									}
									
								} else if (rows[i].state == 0){
									
									changeState(1, rows[i].applianceID);
									
								}//if(currPrice > rows[i].threshold)
						
						
								
	/*							console.log("There are " + rows.length + " rows.");
								
								console.log('The ID is: ', rows[i].applianceID);
					
								console.log('The name is: ', rows[i].name);
					
								console.log('The power is: ', rows[i].power);
					
								console.log('The policy is: ', rows[i].onOffPolicy);
					
								console.log('The threshold is: ', rows[i].threshold);
					
								console.log('The frequency is: ', rows[i].frequency);
					
								console.log('The state is: ', rows[i].state);
*/							} else {}
						}
			
						connection.release();
					
					}//else
			
				});//connection.query
		
			});//pool.connection
			
		} else {}
		
	});//yql.execute
	
}
//The following intervals can be set for testing purposes

//setInterval(testFunc0,5000);

//setInterval(testFunc1,5000);

//setInterval(testFunc2,5000);

//setInterval(testFunc3,5000);

setInterval(testFunc4,5000);
