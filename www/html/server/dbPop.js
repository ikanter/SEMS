var http	= require("http");
var request = require("requestretry");
var mysql	= require('mysql');
var yql = require('yql-node').formatAsJSON();
var requests = require("request");
var csv = require("csvtojson");

//Console decoration for readability, alerts, categories, etc...
var BRIGHT = "\x1b[1m"
var DIM = "\x1b[2m"
var UNDERSCORE = "\x1b[4m"
var BLINK = "\x1b[5m"
var REVERSE = "\x1b[7m"
var HIDDEN = "\x1b[8m"
var CANCEL = "\x1b[0m";
var RED = "\x1b[31m";
var GREEN = "\x1b[32m";
var YELLOW = "\x1b[33m";
var BLUE = "\x1b[34m";
var MAGENTA = "\x1b[35m";
var CYAN = "\x1b[36m";
var WHITE = "\x1b[37m";
var BGRED = "\x1b[41m";
var BGGREEN = "\x1b[42m";
var BGYELLOW = "\x1b[43m";
var BGBLUE = "\x1b[44m";
var BGMAGENTA = "\x1b[45m";
var BGCYAN = "\x1b[46m";
var BGWHITE = "\x1b[47m";





//Credentials for MySQL Database Connection
var pool = mysql.createPool({
	host     : 'localhost',
	user     : 'root',
	password : 'root',
	database : 'test'
});

function getDateFormatted(date){ //Larry: This returns a very specific format which is needed for the nyiso api url

	date = new Date(date);
	var thisMonth = date.getMonth()+1;
	
	if (thisMonth<10){
		
		thisMonth = '0' +  thisMonth;
		
	}
	
	var dateFormatted = date.getFullYear() + '' + thisMonth;
	
	if (date.getDate()<10){
		
		dateFormatted = dateFormatted + '0' + date.getDate();
		
	}else{
		
		dateFormatted = dateFormatted + '' + date.getDate();
		
	}
	
	//Larry: returns the date in the form: '20170820' if the date were August 20th 2017
	
	return dateFormatted;
	
}

function dbForecastPop(){ //Populates a db table with the next day's projected prices
	
	var newDate;
	setTomorrowFromToday();//Sets newDate appropriately						   
	newDate = getDateFormatted(newDate);//Larry: As the name implies, this will format in a manner suitable for the url query
	
	/*  //Larry: Url build-up explanation
	//The first variable is the queried api, which requires the formatted date to be current for the commented out url
	//The following variables filter, refine and optimize the results of the query
	//so that instead of receiving 1000's of data-points in no particular order from the api,
	//everytime the interval is hit, only a limited number of the most current results are returned, in
	//a specific order and from a specific area
	
	//If desired, extensive documentation can be found online by googling "YQL optimization"
	*/
	var nyMIS = "http://mis.nyiso.com/public/csv/damlbmp/" + newDate + "damlbmp_zone.csv"; //Where the date formatted would be one day ahead for a future projected lbmp
	var filter = " and col1 = 'N.Y.C.'";
	var sort = "sort(field='col0',descending='false')";
	var range = "tail(count=24)";
	var nyUrl = "select * from csv where url = '" + nyMIS + "'" + filter + "|" + sort + "|" + range + "";
	
	yql.execute(nyUrl, function(error, response){//Larry: Added yql functionality
												  //This command is made possible by the above package, 'yql-node'
												  //which utilizes yahoo's query service to make the necassary query
												  //a great deal simpler to use in node.js
												  
		if(error) throw error;
		
		var NyObject = response.query.results.row;
		
		dropOldArchive();//Defined below
		copyPresentArchiveFormat();//Defined below
		archiveProjectionData();//Defined below
		console.log('Updating Projection...\n');

		for (var i = 0;i < NyObject.length;i++){
			
			populateDatabase(NyObject[i].col0, NyObject[i].col3, i + 1);//Defined below
			
			if((i + 1) % 2 == 0){//Alternates colors for readability
				console.log(`\t ${GREEN}${i + 1}:\tInserted Data for Timestamp\t->\t ${NyObject[i].col0},\tProjected Price Data\t->\t ${NyObject[i].col3}${CANCEL}`);
			}else{
				console.log(`\t ${CANCEL}${i + 1}:\tInserted Data for Timestamp\t->\t ${NyObject[i].col0},\tProjected Price Data\t->\t ${NyObject[i].col3}${CANCEL}`);
			}
			
		}
		
		console.log(`\n\t\t${BRIGHT}${UNDERSCORE}${YELLOW}Update in 24 hours${CANCEL}\n`);

	});	//yql.execute
	
	function dropOldArchive(){//Get rid of old table
		
		console.log('Dropping old archive...');
		
		pool.getConnection(function(err, connection) {
					
			if (err) throw err;
			//Larry: The following is akin to a prepared query 
			//DROP TABLE archivedFutureNyPrices;
			var sql = "DROP TABLE ??";
			var inserts = ['archivedFutureNyPrices'];
			sql = mysql.format(sql, inserts);
			connection.query(sql);
			connection.release();
				
		});
		
	}
	
	function copyPresentArchiveFormat(){//Reproduce the appropriate format
		
		console.log('Formatting appropriate archive...');

		pool.getConnection(function(err, connection) {
					
			if (err) throw err;
			//Larry: The following is akin to a prepared query 
			//CREATE TABLE archivedFutureNyPrices LIKE futureNyPrices;
			var sql = "CREATE TABLE ?? LIKE ??";
			var inserts = ['archivedFutureNyPrices', 'futureNyPrices'];
			sql = mysql.format(sql, inserts);
			connection.query(sql);
			connection.release();
				
		});
		
	}
	
	function archiveProjectionData(){//Record the appropriate data
		
		console.log('Copying Data...');

		pool.getConnection(function(err, connection) {
					
			if (err) throw err;
			//Larry: The following is akin to a prepared query 
			//INSERT INTO archivedFutureNyPrices SELECT * FROM futureNyPrices;
			var sql = "INSERT INTO ?? SELECT * FROM ??;";
			var inserts = ['archivedFutureNyPrices', 'futureNyPrices'];
			sql = mysql.format(sql, inserts);
			connection.query(sql);
			connection.release();
				
		});
		
	}
	
	function populateDatabase(date,price,i){//Populate the updated projection
		
		pool.getConnection(function(err, connection) {
					
			if (err) throw err;
			//Larry: The following is akin to a prepared query 
			//UPDATE futureNyPrices SET date = 'that', futurePrice = 1.2 WHERE ID = 1 LIMIT 1;
			var sql = "UPDATE ?? SET ?? = ?, ?? = ? WHERE ?? = ?";
			var inserts = ['futureNyPrices', 'date', date, 'futurePrice', price, 'ID', i];
			sql = mysql.format(sql, inserts);
			connection.query(sql);
			connection.release();
				
		});
		
	}
	
	function setTomorrowFromToday(){//This function will set newDate to be exactly one day in the future
	
		var globalDate = new Date();
		newDate = new Date(globalDate.setTime(globalDate.getTime() + 86400000));
		console.log('\n');	
		
	}//setTomorrowFromToday()
	
}//function dbForecastPop(){

function dbCurrentPop(){//populates the db with 100 of the most recent data points
	
	var newDate = new Date();
	newDate = getDateFormatted(newDate);
	var nyMIS = "http://mis.nyiso.com/public/csv/realtime/" + newDate + "realtime_zone.csv"; //This url should most recent set of data '''Testing'''
	var filter = " and col1 = 'N.Y.C.'";
	var sort = "sort(field='col0',descending='false')";
	var range = "tail(count=100)";
	var nyUrl = "select * from csv where url = '" + nyMIS + "'" + filter + "|" + sort + "|" + range + "";
	yql.execute(nyUrl, function(error, response){
		
		if (error) throw error;
		
		for(i = 0;i<response.query.results.row.length;i++){
			
			//console.log(i);
			//console.log(`${i+1}\t<-->\t${new Date(response.query.results.row[i].col0).getTime()}\t<-->\t${response.query.results.row[i].col3}`);
			var time = response.query.results.row[i].col0;
			var price = Number(response.query.results.row[i].col3);
			
			if((i + 1) % 2 == 0){//Alternates colors for readability
				console.log(`\t ${CANCEL}${i + 1}:\tInserted Data for Timestamp\t->\t ${time},\tArchived Price Data\t->\t ${price}${CANCEL}`);
			}else{
				console.log(`\t ${GREEN}${i + 1}:\tInserted Data for Timestamp\t->\t ${time},\tArchived Price Data\t->\t ${price}${CANCEL}`);
			}
			
			currPop(time, price, i+1);//function defined below

		}
		
		console.log(`\n\t\t${BRIGHT}${UNDERSCORE}${YELLOW}Update in 15 minutes${CANCEL}\n`);
		
	});
		
	function currPop(date, price, id){//takes the date and price generated by the query above
				
		pool.getConnection(function(err, connection) {
			if (err) throw err;
			//Larry: The following is akin to a prepared query 
			//UPDATE futureNyPrices SET date = 'that', futurePrice = 1.2 WHERE ID = 1 LIMIT 1;
			var sql = "UPDATE ?? SET ?? = ?, ?? = ? WHERE ?? = ?";
			var inserts = ['currentNyPrices', 'date', date, 'price', price, 'id', id];
			sql = mysql.format(sql, inserts);
			connection.query(sql);
			connection.release();
			
		});//pool.getConnection(function(err, connection){
		
	}//function currPop(date, price, id){
	
}//function dbCurrentPop(){

function altApiTest(){//This was incorporated into the main server.js
	var i = 0;
	theTime = [];
	thePrice = [];
	theLocation = [];
	url = 'http://mis.nyiso.com/public/realtime/realtime_zone_lbmp.csv';
	csv()
	.fromStream(requests.get(url))
	.on('csv', (csvRow)=>{
			//~ i++;
			//~ console.log(`Iteration: ${i}`);
			//~ if (i%9 == 0){
				//~ console.log(`\t\tDate: ${csvRow[0]} : Location: ${csvRow[1]} : PID: ${csvRow[2]} : LMP: ${csvRow[3]} : Deviation: ${csvRow[4]} : csvRow5: ${csvRow[5]} `);
				thePrice.push(csvRow[3]);	
				theTime.push(csvRow[0]);
				theLocation.push(csvRow[1]);
				//~ console.log(`\t\ttheLocation: ${theLocation[0]} : theTime: ${theTime[0]} : thePrice: ${thePrice[0]}\n\t\ttheLocation: ${theLocation[1]} : theTime: ${theTime[1]} : thePrice: ${thePrice[1]}\n\t\ttheLocation: ${theLocation[2]} : theTime: ${theTime[2]} : thePrice: ${thePrice[2]}\n`);
				//~ console.log(theLocation.length);
				
				
			//~ }
	})
		.on('done',(error)=>{
			
				drop();
				
				for(j = 0;j<theLocation.length;j++){
					//~ console.log(`\t\ttheLocation: ${theLocation[j]}\t:\ttheTime: ${theTime[j]}\t:\tthePrice: ${thePrice[j]}\n`);
					time = theTime[j];
					city = theLocation[j];
					lmp = Number(thePrice[j]);

					create();
		
					pop(time, city, lmp);

				}
				
				//~ console.log(`\t\tcurrPrice: ${currPrice}\n`);
				//~ console.log(`${Number(thePrice[0])}`);
				//~ console.log(`${theTime[0]}`);
				//~ console.log(`thePrice: ${thePrice} : theTime: ${theTime} `);
		});
		
		function drop(){
			
			pool.getConnection(function(err, connection) {
				if (err) throw err;
				var sql	= "DROP TABLE multi";
			
				connection.query(sql);
				
				connection.release();
				
			});	
			
		}
		
		function create(){
			
			pool.getConnection(function(err, connection) {
				
				if (err) throw err;
				//~ 
				//~ //Larry: The following is akin to a prepared query 
				//~ 
				var sql	= "CREATE TABLE multi ( id int NOT NULL AUTO_INCREMENT, time varchar(255), city varchar(255), lmp float(10), PRIMARY KEY (id) )";
				//~ 
				//var sql = "UPDATE ?? SET ?? = ?";
				//~ 
				//~ var inserts = ['multi', 'time', 'city', 'lmp', time, city, lmp];
				//~ 
				//~ sql = mysql.format(sql, inserts);
				
				connection.query(sql);
				
				connection.release();
				
			});	
					
		}
		
		function pop(time, city, lmp){
			
			pool.getConnection(function(err, connection) {
				
				console.log(`\t\ttheLocation: ${city}\t:\ttheTime: ${time}\t:\tthePrice: ${lmp}\n`);
				if (err) throw err;
				//~ 
				//~ //Larry: The following is akin to a prepared query 
				//~ 
				var sql	= "INSERT INTO ?? (??, ??, ??) VALUES (?,?,?)";
				//~ 
				//var sql = "UPDATE ?? SET ?? = ?";
				//~ 
				var inserts = ['multi', 'time', 'city', 'lmp', time, city, lmp];
				
				sql = mysql.format(sql, inserts);
				
				connection.query(sql);
				
				connection.release();
				
			});
			
		}	
}



altApiTest(); //This ended up being used in the main server.js script
//~ dbCurrentPop();
//~ dbForecastPop();
//~ setInterval(dbForecastPop,86400000);	//86,400,000ms = 1day
//~ setInterval(dbCurrentPop,900000);	//900,000ms = 15min


