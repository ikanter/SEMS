var http	= require("http");
var request = require("requestretry");
var mysql	= require('mysql');



//Credentials for MySQL Database Connection
var pool = mysql.createPool({
	host     : 'localhost',
	user     : 'root',
	password : 'mk530',
	database : 'test'
});



//pool.end();



//Prepared SQL queries
var createLMP	= "CREATE TABLE LMP(Date char(16), LMP char(10))";
var deleteLMP	= "DROP TABLE IF EXISTS LMP";
var insertLMP	= "INSERT INTO LMP (Date, LMP) VALUES(?,?)";					
var insertDate  = "INSERT INTO Dates (Date) VALUES(?)";
var insertState = "INSERT INTO State (dateID, applianceID, state, policy) VALUES(?,?,?,?)";		
var selectAppliance = "SELECT applianceID, state, onOffPolicy FROM userSettings";
var selectPeriods	= "SELECT userSettings.applianceID, day, beginTime, endTime, onOffPolicy, threshold, frequency FROM userSettings LEFT JOIN offPeriods ON offPeriods.applianceID = userSettings.applianceID ORDER BY applianceID";
var updateState		= "UPDATE userSettings SET state = ? WHERE applianceID = ?";
var selectPolicy	= "SELECT onOffPolicy, threshold FROM userSettings WHERE applianceID = ?";
var selectFrequency = "SELECT Frequency FROM Frequency";
var selectFreqThreshold = "SELECT applianceID, Frequency FROM userSettings";
//pool.query(deleteLMP);
//pool.query(createLMP);



//Authentication for ISO-NE API
var username = "smarthomeenergymanagementsystm@gmail.com";
var password = "SHEMSshgl530";


//Set options to retrive info from ISO-NE API
var options = {
	"rejectUnauthorized": false,
	url: "https://webservices.iso-ne.com/api/v1.1/fiveminutelmp/current/location/4004.json",
	method: 'GET',
	maxAttempts: 5,
	retryDelay: 5000,
	retryStrategy: request.RetryStrategies.HTTPOrNetworkError,
	auth: {
		user: username,
		password: password
	}
};

var id;
var pastDate = "0";
var lmp;
//var policyReturn;

//Retrive info from ISO-NE API and puts into database
function func(){
	request(options, function(err, res, body){ //body = JSON object returned by API call
		if(err){
			console.log(err);
			return;
		}
	
		var jsonObject = JSON.parse(body); //Save JSON object
		var day = jsonObject.FiveMinLmp[0].BeginDate.substring(0,16); //Save the Date
		lmp = jsonObject.FiveMinLmp[0].LmpTotal; //Save the LMP	
		
		if(pastDate == day){
			return;
		}
		else{
			pastDate = day;
		}

		//Connect to DB
		pool.getConnection(function(err, connection){
			if(err){
				console.log('Error connecting to DB');
				console.log(err);
				return;
			}
			console.log('Connection established');
					
			//Query the DB 
			connection.query(insertLMP,[day, lmp],function(err, rows, fields){
//				connection.release();
				if(!err){
					console.log('Date: ', day);
					console.log('LMP:  ', lmp);	
					id = rows.insertId;
					console.log(id);
					
					connection.query(selectAppliance, function(err, rows, fields){	
						for(var i = 0; i < rows.length; i++){
							connection.query(insertState, [id, rows[i].applianceID, rows[i].state, rows[i].onOffPolicy],function(err, rows, fields){
//								connection.release();
								if(!err){
									console.log('Inserted stuff into DB');
								}
								else{
									console.log('Error while performing inserState Query');
								}
							});
						}
					});	
					connection.release();
				//	return;
				}
				else{
					console.log('Error while performing insertLMP Query.');	
				}
			});	
		});
	});
}

var j=300;
var currentFrequency;

function getFrequency(){
	pool.getConnection(function(err, connection){
		if(err){
			console.log('Error connecting to DB');
			console.log(err);
			return;
		}
		if(j%20==0){
			console.log(' ');
			console.log('Connection to Frequency DB established:', j);
			console.log(' ');
		}
		connection.query(selectFrequency, function(err, rows, fields){
			currentFrequency = rows[j].Frequency;
			console.log('                                                     Current Frequency: ');
			console.log('                                                                          ', currentFrequency);
			j++;
			if(j>=rows.length){j=0;}
		});
		connection.release();
	});
}

var ret, pre;

function checkTime(){
	var id;
	var state;
	//Connect to DB
	pool.getConnection(function(err, connection){
		if(err){
			console.log('Error connecting to DB');
			console.log(err);
			return;
		}
//		console.log('Connection established');
					
		//Query the DB 
		connection.query(selectPeriods, function(err, rows, fields){
			for(var i = 0; i < rows.length; i++){
				
				// Check Policy
				pre = ret
				if(rows[i].onOffPolicy == 0){
					ret = 1;
				}
				else if(rows[i].onOffPolicy == 1){
					if(rows[i].threshold < lmp){	
						ret = 0;
					}
					else{
						ret = 1;
					}
				}
				else if(rows[i].onOffPolicy == 2){
					ret = 0;
				}
				else if(rows[i].onOffPolicy == 3){
					if(rows[i].frequency > currentFrequency){
						ret = 0;
					}
					else{
						ret = 1;
					}
				}


				// Check Period
				if(i == 0){
					id = rows[i].applianceID;
					state = checkPeriod(rows[i], connection);	
					pre = ret;
				}

				if(id == rows[i].applianceID){
					if(state != 0){
						state = checkPeriod(rows[i], connection);		
					}
				}
				else{
					if(state == 1){
						state = pre;
					}
					connection.query(updateState, [state, id]);	
				//	console.log("2 Updated appliance: " + id + " to state: " + state);
					id = rows[i].applianceID;
					state = checkPeriod(rows[i], connection);	
				}
			}

			if(state == 1){
				state = ret;		
			}

			connection.query(updateState, [state,id]);
		//	console.log("1 Updated appliance: " + id + " to state: " + state);
		
		});
		connection.release();
	});


}


function checkPeriod(row, connection){
	var d = new Date();
	
	var day = row.day;
	if(day == null){
		return 1;
	}
	var arr = day.split(',');
	var flag = 0;

	for(var j = 0; j < arr.length; j++){

		// Change day to integer
		if(arr[j]== 'sun') arr[j] = 0;
		else if(arr[j] == 'mon') arr[j] = 1;
		else if(arr[j] == 'tue') arr[j] = 2;
		else if(arr[j] == 'wed') arr[j] = 3;
		else if(arr[j] == 'thu') arr[j] = 4;
		else if(arr[j] == 'fri') arr[j] = 5;
		else if(arr[j] == 'sat') arr[j] = 6;

		if(arr[j] == d.getDay()){
			flag = 1;
			// Check if start time is less than current time
			var start = row.beginTime.split(':');
			var end = row.endTime.split(':');
			if(start[0] < d.getHours()){
				if(end[0] > d.getHours()){
					return 0;
				}
				else if((end[0] == d.getHours()) && (end[1] >= d.getMinutes())){
					return 0;
				}
				else{
					return 1;
				}	
			}
			else if((start[0] == d.getHours()) && (start[1] <= d.getMinutes())){
				if(end[0] > d.getHours()){
					return 0;
				}
				else if((end[0] == d.getHours()) && (end[1] >= d.getMinutes())){
					return 0;
				}
				else{
					return 1;
				}
			}
			else{	
				return 1;
			}
		}
	}
	if(flag == 0){
		return 1;	
	}
}

func();
getFrequency();
//Set interval to check on/off period every 1min
//setInterval(checkTime, 60000);	//60,000ms = 1min
setInterval(checkTime, 500);		//1,000ms = 1s

//Set interval to call func every 5mins 
setInterval(func,300000);	//300,000ms = 5min
//Set interval to call func every 0.5 second
setInterval(getFrequency,500);
