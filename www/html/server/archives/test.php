

//The intervals at the bottom of the page can be set to see the code in action,

//simply uncomment the intervals, and then navigate to the terminal and type 'node test.js' to run the code 

//(Similar to 'python myCode.py' to run a python code file)

var http        = require("http");
var request = require("requestretry");
var mysql       = require('mysql');



//Credentials for MySQL Database Connection
var pool = mysql.createPool({
        host     : 'localhost',
        user     : 'root',
        password : 'mk530',
        database : 'test'
});

var i = 1;

function testFunc0 () { //This constitutes an easy-to-follow working example of making an UPDATE query

	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		connection.query('UPDATE ?? SET ?? = ? WHERE ?? = ?', ['userSettings', 'threshold', i,  'applianceID', i]);
	
				connection.release();
			
	});
			i += 1;
	
}

function testFunc1 () { //This constitutes an alternate working example of making an UPDATE query
                        //Using this method gives full control over the query sent to the db, preventing SQL injection

	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		var sql = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
		
		var inserts = ['userSettings', 'threshold', i,  'applianceID', i];
		
		sql = mysql.format(sql, inserts);
		
		connection.query(sql);
	
				connection.release();
			
	});
			i += 1;
	
}

function testFunc2 () { //This constitutes an easy-to-follow working example of a SELECT query

	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
	
		connection.query('SELECT * FROM ?? WHERE ?? = ?', ['userSettings', 'applianceID', i], function(err, rows, fields) {
	
			if (err) throw err;
			
			if (rows.length == 0) {
			
				console.log('Row condition met: undefined');
			
			} else {
			
					
				console.log('The ID is: ', rows[0].applianceID);
	
				console.log('The name is: ', rows[0].name);
	
				console.log('The power is: ', rows[0].power);
	
				console.log('The policy is: ', rows[0].onOffPolicy);
	
				console.log('The threshold is: ', rows[0].threshold);
	
				console.log('The frequency is: ', rows[0].frequency);
	
				console.log('The state is: ', rows[0].state);
	
				connection.release();
			
			
			}
			i += 1;
	
		});
	
	});

}

function testFunc3 () { //This constitutes an easy-to-follow working example of a SELECT query
                        //Using this method gives full control over the query sent to the db, preventing SQL injection

	pool.getConnection(function(err, connection) {
	
		if (err) throw err;
		
		var sql = "SELECT * FROM ?? WHERE ?? = ?";
		
		var inserts = ['userSettings', 'applianceID', i];
		
		sql = mysql.format(sql, inserts);
	
		connection.query( sql, function(err, rows, fields) {
	
			if (err) throw err;
			
			if (rows.length == 0) {
			
				console.log('Row condition met: undefined');
			
			} else {
			
					
				console.log('The ID is: ', rows[0].applianceID);
	
				console.log('The name is: ', rows[0].name);
	
				console.log('The power is: ', rows[0].power);
	
				console.log('The policy is: ', rows[0].onOffPolicy);
	
				console.log('The threshold is: ', rows[0].threshold);
	
				console.log('The frequency is: ', rows[0].frequency);
	
				console.log('The state is: ', rows[0].state);
	
				connection.release();
			
			
			}
			i += 1;
	
		});
	
	});

}

//The following intervals can be set for testing purposes

//setInterval(testFunc0,5000);

//setInterval(testFunc1,5000);

//setInterval(testFunc2,5000);

setInterval(testFunc3,5000);



