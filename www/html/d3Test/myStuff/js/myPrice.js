var parseDate = d3.timeParse("%m/%d/%Y %H:%M");

		var svg = d3.select("body")
						.append("svg")
						.attr("height","100%")
						.attr("width","100%");
		
		var margin = {left:90,right:50,top:100,bottom:0};
		
		var chartGroup = svg.append("g")
								.attr("transform","translate("+margin.left+","+margin.top+")");

d3.json("php/getData.php")
	.get(callback);
	
	function callback(error,data){
			
		//~ console.log(`This is data: ${parseDate(data[0].date)} & ${Number(data[0].futurePrice) + 1}`)// navigates the data
		
		//~ data = data.slice(12);//Halves the data
		
		var height = 600/1.2;
		var width = 1000/1.2;
		
		var max = d3.max(data, function(d){ return Number(d.futurePrice); });
		var minDate = d3.min(data, function(d){ return parseDate(d.date); });
		var maxDate = d3.max(data, function(d){ return parseDate(d.date); });
		
		var y = d3.scaleLinear()
					.domain([0,max])
					.range([height,0]);
		var x = d3.scaleTime()
					.domain([minDate,maxDate])
					.range([0,width]);
		var yAxis = d3.axisLeft(y);
		var xAxis = d3.axisBottom(x)
						.ticks(12);
		//~ 
		//~ var svg = d3.select("body")
						//~ .append("svg")
						//~ .attr("height","100%")
						//~ .attr("width","100%");
		//~ 
		//~ var margin = {left:90,right:50,top:100,bottom:0};
		//~ 
		//~ var chartGroup = svg.append("g")
								//~ .attr("transform","translate("+margin.left+","+margin.top+")");
		//~ 
		var line = d3.line()
						.curve(d3.curveBasis)
						.x(function(d){return x(parseDate(d.date)); })
						.y(function(d){return y(Number(d.futurePrice)); });
						
		chartGroup.append("path")
			.attr("transform","translate(1000,"+height+")")
			.attr("transform","rotate(-200)")
				.transition()
					.ease(d3.easeSin)
					.duration(1000)
					.delay(400)
			.attr("transform","translate(0,0)")
			.attr("d",line(data));
		chartGroup.append("g")
			.attr("class","x axis")
				.transition()
					.ease(d3.easeElastic)
					.duration(500)
					.delay(650)
			.attr("transform","translate(0,"+height+")")
				.call(xAxis.tickFormat(d3.timeFormat("%H:%M")));
		chartGroup.append("g")
			.attr("class","y axis")
			.attr("transform","translate(0,"+width+")")
				.transition()
					.ease(d3.easeElastic)
					.duration(500)
					.delay(650)
			.attr("transform","translate(0,0)").call(yAxis);
		
		svg.append("text")
			.transition().ease(d3.easeElastic).duration(500).delay(450)
			.attr("transform","translate(" + ((width/2) + margin.left ) + " , " + (height + margin.top + 50) + ")")
			.style("text-anchor","middle")
			.text("Date");
			

		svg.append("text")
			.transition().ease(d3.easeElastic).duration(500).delay(450)
			.attr("transform","rotate(-90)")
			.attr("y",+ margin.left / 3 )
			.attr("x",0 - (height / 2))
			.attr("dy","1em")
			.style("text-anchor","middle")
			.text("Price ($)");

		svg.append("text")
			.attr("transform","translate(" + ((width/2) + margin.left ) + " , " + (margin.top / 2) + ")")
			.classed("title",true)
			.style("text-anchor","middle")
			.text("Today's Projected Price");

	}
	
