<?php

        //Normally defined in a function
        define("DB_SERVER", "localhost");
        define("DB_USER", "root");
        define("DB_PASS", "root");
        define("DB_NAME", "test");

    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    
    if(mysqli_connect_errno()) {
      //Self-Explanatory
      $msg = "Database connection failed: ";
      $msg .= mysqli_connect_error();
      $msg .= " (" . mysqli_connect_errno() . ")";
      exit($msg);
    }
    //The Query
    $sql = "SELECT * FROM archivedFutureNyPrices";
    //Pass in the connection
    $result = mysqli_query($connection, $sql);
    // For UPDATE statements, $result is true/false
    if(!$result) {//Select query failed
      echo mysqli_error($connection);
      db_disconnect($connection);
      exit;
    } 
    
    $data = array();
    
    for ($x = 0; $x < mysqli_num_rows($result); $x++){
		
		$data[] = mysqli_fetch_assoc($result);
		
	}
	
	echo json_encode($data);
    
    if(isset($connection)) {
      mysqli_close($connection);
    }
?>
