//Working with data to produce a barchart

var bardata = [20*3, 30*3, 105*3, 15*3, 85*3, 44*3, 92*3, 23*3, 12*3];

var height = 400,
    width = 600,
    barWidth = 50,
    barOffset = 5;

d3.select('#chart').append('svg')
    .attr('width', width)
    .attr('height', height)
    .style('background', '#C9D7D6')
    .selectAll('rect').data(bardata)
    .enter().append('rect')
        .style('fill', '#C61C6F')
        .attr('width', barWidth)
        .attr('height', function(d) {
            return d;
        })
        //the offset so they're not stacked on top of each other
        .attr('x', function(d,i) {
            return i * (barWidth + barOffset);
        })
        .attr('y', function(d) {//the offset
            return height - d;
        })
