var bardata = [];

for (var i=0; i < 50; i++) {
    bardata.push(Math.random())
}

var height = 400,
    width = 600,
    barWidth = 50,
    barOffset = 5;

var colors = d3.scale.linear()//decided by horizontal position
.domain([0, bardata.length*.5, bardata.length])
.range(['#C0FFC5','#C0C3FF', '#FFC0CB'])

//~ var colors = d3.scale.linear()//decided by amplitude
//~ .domain([0, d3.max(bardata)*.33, d3.max(bardata)*.66, d3.max(bardata)])
//~ .range(['#29B54F','#C6931C', '#D2268C', '#2C4399'])

var yScale = d3.scale.linear()
        .domain([0, d3.max(bardata)])
        .range([0, height]);

var xScale = d3.scale.ordinal()
        .domain(d3.range(0, bardata.length))
        .rangeBands([0, width])

d3.select('#chart').append('svg')
    .attr('width', width)
    .attr('height', height)
    .selectAll('rect').data(bardata)
    .enter().append('rect')
		//~ .style('fill', colors)
        .style('fill', function(d,i) {//colors are decided by horizontal position
            return colors(i);
        })
        .attr('width', xScale.rangeBand())
        .attr('height', function(d) {
            return yScale(d);
        })
        .attr('x', function(d,i) {
            return xScale(i);
        })
        .attr('y', function(d) {
            return height - yScale(d);
        })
