//~ Pretty self-explanatory
//~ Uses data to append colors and bg

var myStyles = [
  { width: 200,
    bgcolor: '#2176C7',
    color: '#D11C24'},
  { width: 230,
    bgcolor: '#595AB7',
    color: '#BD3613'},
  { width: 260,
    bgcolor: '#C61C6F',
    color: '#A57706'},
  { width: 290,
    bgcolor: '#D11C24',
    color: '#2176C7'},
  { width: 260,
    bgcolor: '#BD3613',
    color: '#595AB7'},
  { width: 230,
    bgcolor: '#A57706',
    color: '#C61C6F'}
];

//~ d3.selectAll('.item')
  //~ .data(myStyles)
  //~ .style({
    //~ 'color': 'white',
    //~ 'background' : function(d) {
      //~ return d.color;
    //~ },
    //~ width : function(d) {
      //~ return d.width + 'px';
    //~ } 
  //~ })
  
d3.selectAll('.item')
  .data(myStyles)
  .style({
	  
    'color': function(d) {
      return d.color;
    },
    
    'background' : function(d){
		return d.bgcolor;
	},
    
    width : function(d) {
      return d.width + 'px';
    } 
  })
