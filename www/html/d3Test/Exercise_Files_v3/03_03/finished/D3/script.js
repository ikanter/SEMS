var base = 100;

d3.select('#chart')//select the chart
  .append('svg')//append the svg element
    .attr('width', (6*base))//add attributes
    .attr('height', (4*base))
    .style('background', "#93A1A1")//can be used in a css doc if desired
  .append("rect")//placed in the current selection
	//rect requires four mandatory attributes  
    .attr('x', (2*base))
    .attr('y', (1*base))
    .attr('height', (2*base))
    .attr('width', (2*base))
    .style('fill', '#CB4B19')
  d3.select('svg')//have to select the svg to not overwrite
    .append('circle')
    .attr('cx','300')//three mandatory attributes
    .attr('cy','200')
    .attr('r', base)
    .style('fill', '#840043')
