//~ Selections, toggling classes, etc...

  //~ This would add the class, myClass, 
  //~ and take away the item class, 
  //~ but keep in mind that you actually 
  //~ have to add the class on a stylesheet
//~ d3.selectAll('.item')
  //~ .attr('class', 'myClass')
  
  //~ This instead toggles the class, which 
//~ is in addition to the item class
//~ d3.selectAll('.item:nth-child(3n)')
  //~ .classed('highlight', true)
  
  //~ You can also pass in an object for multiple commands
//~ d3.selectAll('.item:nth-child(3n)')
  //~ .classed({
    //~ 'highlight': true,
    //~ 'item': false,
    //~ 'bigger': true
  //~ })

//~ d3.selectAll('.item:c')
  //~ .style({
    //~ 'background': '#268BD2',
    //~ 'padding': '10px',
    //~ 'margin' : '5px',
    //~ 'color' : '#EEE8D5'
  //~ })
  //~ 

