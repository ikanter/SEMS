var parseDate = d3.timeParse("%m/%d/%Y");

d3.csv("prices.csv")
	.row(function(d){ 
		return {month: parseDate(d.month), price:Number(d.price.trim().slice(1))};
	 })//Runs a transformation at each data point
	.get(function(error, data){
		if (error) throw error;
		console.log(data);
	})
