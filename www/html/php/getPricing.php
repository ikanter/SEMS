<?php
	$servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "test";

	$conn = mysqli_connect($servername, $username, $password, $dbname);
	if(!$conn){
		die("Connection failed: " . mysqli_connect_error());
	}
	
	// Query the price information from the database
	$priceQuery = "SELECT Date, LMP FROM LMP;";
	$queryResult = mysqli_query($conn, $priceQuery);
	
	// Output the price information to a script	
	echo "pricingData = [";
	while($row = mysqli_fetch_array($queryResult)){
		$date = $row["Date"];
		$lmp = $row["LMP"];
	//	$forecast = $row["ForecastLMP"];

		$tuple = array("datestr" => $date, "lmp" => $lmp);
		echo json_encode($tuple) . ",";
	}
	echo "];";

	mysqli_close($conn);
?>
