<?php
        echo "<script type='text/javascript'>";
        echo "window.close();";
        echo "</script>";
        echo "<h4>Required Field</h4>";

        //Normally defined in a function
        define("DB_SERVER", "localhost");
        define("DB_USER", "root");
        define("DB_PASS", "root");
        define("DB_NAME", "test");

    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    
    if(mysqli_connect_errno()) {
      //Self-Explanatory
      $msg = "Database connection failed: ";
      $msg .= mysqli_connect_error();
      $msg .= " (" . mysqli_connect_errno() . ")";
      exit($msg);
    }
    //The Query
    $sql = "UPDATE userSettings SET ";
    $sql .= "onOffPolicy=2";
    //Pass in the connection
    $result = mysqli_query($connection, $sql);
    // For UPDATE statements, $result is true/false
    if($result) {
      return true;
    } else {
      // UPDATE failed
      echo mysqli_error($connection);
      db_disconnect($connection);
      exit;
    }
    
    if(isset($connection)) {
      mysqli_close($connection);
    }
?>

