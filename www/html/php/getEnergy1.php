<?php
	//ini_set('memory_limit', '1024M');
	$servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "test";

	$conn = mysqli_connect($servername, $username, $password, $dbname);
	if(!$conn){
		die("Connection failed: " . mysqli_connect_error());
	}

	// Query the price information from the database
	$selectQuery = "SELECT date, LMP, GROUP_CONCAT(State.applianceID SEPARATOR ',') AS applianceID,
								GROUP_CONCAT(name SEPARATOR ',') AS name,
								GROUP_CONCAT(State.state SEPARATOR ',') AS state,
								GROUP_CONCAT(power SEPARATOR ',') AS power ,
								GROUP_CONCAT(policy SEPARATOR ',') AS policy,
								GROUP_CONCAT(user SEPARATOR ',') AS user
								FROM State, LMP, userSettings 
								WHERE LMP.dateID = State.dateID 
									and userSettings.applianceID = State.applianceID 
								GROUP BY State.dateID;";
	
	$queryResult1 = mysqli_query($conn, $selectQuery);
	
	$state1 = array();
	$power1 = array();
	$applianceID1 = array();
			
	echo "energyData1 = [";
	// Output the price information to a script
	while($row1 = mysqli_fetch_array($queryResult1)){	
		$applianceID1 = explode(',', $row1["applianceID"]);
		$state1  = explode(',', $row1["state"]);
		$power1  = explode(',', $row1["power"]);
		$policy1 = explode(',', $row1["policy"]);
		$name1   = explode(',', $row1["name"]);
		$user1	= explode(',', $row1["user"]);
		$LMP1 = $row1["LMP"];	
	//Peter: Added an if statement to get data for user 1
		for($i = 0; $i < count($applianceID1); $i++){
			if ($user1[$i] == "1") {	
				$tuple1 = array("datestr" => $row1["date"], 
							"applianceID" => $applianceID1[$i],	
							"LMP" => $LMP1,
							"name" => $name1[$i],
							"state" => $state1[$i],
							"power" => $power1[$i],
							"policy" => $policy1[$i]	
							);
											
				echo json_encode($tuple1).",";
			}
		}	
	}
	echo "];";	
	mysql_free_result($queryResult1);
	mysqli_close($conn);
?>
