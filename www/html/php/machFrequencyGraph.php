<?php

define("DB_SERVER", "localhost");
define("DB_USER", "root");
define("DB_PASS", "root");
define("DB_NAME", "test");

$db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

if(mysqli_connect_errno()) {
	$msg = "Database connection failed: ";
	$msg .= mysqli_connect_error();
	$msg .= " (" . mysqli_connect_errno() . ")";
	exit($msg);
}

$sql = "SELECT * FROM Frequency ";

$sql .= "ORDER BY ID ASC";
//echo $sql;
$result_set = mysqli_query($db, $sql);

if (!$result_set) {
	exit("Database query failed.");
}

?>

<!DOCTYPE html>
 <html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>SHEMS Web App</title>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>

		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/index.css">
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="js/newscripts.js"></script>
        <script type="text/javascript">
			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart);
			function drawChart() {
				var data = google.visualization.arrayToDataTable([
				  [<?php echo "'Time'" ?>, <?php echo "'Frequency'" ?>],
				  <?php while($result = mysqli_fetch_assoc($result_set)) { ?>
				  ['<?php echo $result['Time']; ?>', <?php echo $result['Frequency']; ?>],
				  <?php } ?>
				  ['16:49:00',  60]
				]);

			<?php
				 if(isset($db)) {
					mysqli_close($db);
				 }
			?>

				var options = {
				  title: 'Frequency',
				  curveType: 'function',
				  colors: ['green', 'yellow', 'gray'],
				  vAxis: {
					  gridlines: { count: 4 },
					  title: 'Hertz'
					   },
				  hAxis: {
					  title: 'Time (hh:mm:ss)'
					   },
				  annotations: {
				  },
				  legend: { position: 'right' }
				};


				var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

				chart.draw(data, options);
			}

    </script>

	<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>

	<body id="body">
		<header class="container-fluid" id="header">
			<div class="container">
				<a href="#" onClick="return showHome()">
					<h2 id="brand">SHEMS<br>
					<span id="brand-small">Smart Home Energy Management System</span></h2>
				</a>
			</div>
		</header>

		<header class="navbar-inverse" id="navbar">
			<div class="container">
				<ul class="nav navbar-nav">
					<li id="navHome" class="nav-link active"><a href="closeWindow.php">Back</a></li>

				</ul>
			</div>
		</header>


        <div id="curve_chart" style="width: 1350px; height: 750px;"></div>

    </body>
  </html>
