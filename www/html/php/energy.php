<?php
    $servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "test";

	$conn = mysqli_connect($servername, $username, $password, $dbname);
	if(!$conn){
		die("Connection failed: " . mysqli_connect_error());
	}

	$selectQuery = "SELECT DISTINCT (applianceID) FROM onOffTime;";

	$energy = array();

	$queryResult = mysqli_query($conn, $selectQuery);
	while($row = mysqli_fetch_array($queryResult)){
		array_push($energy, array($row[0]));	
		echo "Added New applianceID = ".$row[0]."\n";
	}
	
	echo "Total number of appliances = ".count($energy)."\n";
	
	for($i = 0; $i < count($energy); $i++){
		$selectQuery = "SELECT * FROM onOffTime 
						WHERE applianceID = ".$energy[$i][0]."
						ORDER BY Date ASC;";
		$queryResult = mysqli_query($conn, $selectQuery);

		//Add time and state info to array
		while($row = mysqli_fetch_array($queryResult)){
			$date = new DateTime($row[0]);

			//Round up seconds to mins
			$second = $date->format("s");
			if($second > 30){ //Jump to next minute
				$date->add(new DateInterval("PT".(60-$second)."S"));
			}
			else{ //Round down to 0 second
				$date->sub(new DateInterval("PT".$second."S"));
			}

			//Round up minutes to intervals of 5
			$minute = $date->format("i");
			$minute = $minute % 5;
			$diff = 5 - $minute;
			
			if($diff > 2){ //Round down
				$date->sub(new DateInterval("PT".$minute."M"));
			}
			else{
				$date->add(new DateInterval("PT".$diff."M"));
			}	
			
			echo "Date: ";
			print_r($date);	
			echo "\n";

			array_push($energy[$i], date_format($date, 'Y-m-d H:i:s'));
			array_push($energy[$i], $row[2]);
			echo "Pushed ".date_format($date, 'Y-m-d H:i:s')." onto array ".($i)."\n";
		}	
	}


	$currentDate = new DateTime(date('Y-m-d H:i:s', time()));
	print_r ($currentDate);
	$month = clone $currentDate;
	


	//Round up seconds to mins
	$second = $month->format("s");
	if($second > 30){ //Jump to next minute
		$month->add(new DateInterval("PT".(60-$second)."S"));
	}
	else{ //Round down to 0 second
		$month->sub(new DateInterval("PT".$second."S"));
	}

	//Round up minutes to intervals of 5
	$minute = $month->format("i");
	$minute = $minute % 60;
		
	$month->sub(new DateInterval("P1M"));

	if($minute > 30){ //Round down
		$month->add(new DateInterval("PT".(60-$minute)."M"));
	}
	else{
		$month->sub(new DateInterval("PT".$minute."M"));
	}	

	echo "\n month \n";	
	print_r ($month);
	echo "\n";
	
	$kwh = array();

	while(date_format($month, 'Y-m-d H') != date_format($currentDate, 'Y-m-d H')){
		$tuple = array("datestr" => date_format($month, 'Y-m-d H'), "energy" => 0);
		$month->add(new DateInterval("PT1H"));
		
		array_push($kwh, $tuple);
	}



	echo "\n\n\n";
	echo count($kwh);
	echo "\n\n\n";

	print_r ($energy);


	echo phpversion();	
	echo "\nProgram END\n";

?>
