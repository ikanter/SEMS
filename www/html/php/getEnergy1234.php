<?php
	//ini_set('memory_limit', '1024M');
	$servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "test";

	$conn = mysqli_connect($servername, $username, $password, $dbname);
	if(!$conn){
		die("Connection failed: " . mysqli_connect_error());
	}

	// Query the price information from the database
	$selectQuery = "SELECT date, LMP, GROUP_CONCAT(State.applianceID SEPARATOR ',') AS applianceID,
								GROUP_CONCAT(name SEPARATOR ',') AS name,
								GROUP_CONCAT(State.state SEPARATOR ',') AS state,
								GROUP_CONCAT(power SEPARATOR ',') AS power ,
								GROUP_CONCAT(policy SEPARATOR ',') AS policy,
								GROUP_CONCAT(user SEPARATOR ',') AS user
								FROM State, LMP, userSettings 
								WHERE LMP.dateID = State.dateID 
									and userSettings.applianceID = State.applianceID 
								GROUP BY State.dateID;";
	
	$queryResult = mysqli_query($conn, $selectQuery);
	
	$state = array();
	$power = array();
	$applianceID = array();
	
	
			
	echo "energyData1 = [";
	// Output the price information to a script
	while($row = mysqli_fetch_array($queryResult)){	
		$applianceID = explode(',', $row["applianceID"]);
		$state  = explode(',', $row["state"]);
		$power  = explode(',', $row["power"]);
		$policy = explode(',', $row["policy"]);
		$name   = explode(',', $row["name"]);
		$user	= explode(',', $row["user"]);
		$LMP = $row["LMP"];	
	//Peter: Added an if statement to get data for user 1
		for($i = 0; $i < count($applianceID); $i++){
			if ($user[$i] == "1") {	
				$tuple = array("datestr" => $row["date"], 
							"applianceID" => $applianceID[$i],	
							"LMP" => $LMP,
							"name" => $name[$i],
							"state" => $state[$i],
							"power" => $power[$i],
							"policy" => $policy[$i]	
							);
											
				echo json_encode($tuple).",";
			}
		}	
	}
	echo "];";
//	mysql_free_result($queryResult);

	$queryResult2 = mysqli_query($conn, $selectQuery);
	
	$state2 = array();
	$power2 = array();
	$applianceID2 = array();
	
	echo "energyData2 = [";
	// Output the price information to a script
	while($row2 = mysqli_fetch_array($queryResult2)){	
		$applianceID2 = explode(',', $row2["applianceID"]);
		$state2  = explode(',', $row2["state"]);
		$power2  = explode(',', $row2["power"]);
		$policy2 = explode(',', $row2["policy"]);
		$name2   = explode(',', $row2["name"]);
		$user2	= explode(',', $row2["user"]);
		$LMP2 = $row2["LMP"];	
	//Peter: Added an if statement to get data for user 2
		for($j = 0; $j < count($applianceID2); $j++){
			if ($user2[$j] == "2") {	
				$tuple2 = array("datestr" => $row2["date"], 
							"applianceID" => $applianceID2[$j],	
							"LMP" => $LMP2,
							"name" => $name2[$j],
							"state" => $state2[$j],
							"power" => $power2[$j],
							"policy" => $policy2[$j]	
							);
											
				echo json_encode($tuple2).",";
			}
		}	
	}
	echo "];";
//	mysql_free_result($queryResult2);
	
	$queryResult3 = mysqli_query($conn, $selectQuery);
	
	$state3 = array();
	$power3 = array();
	$applianceID3 = array();
	
	echo "energyData3 = [";
	// Output the price information to a script
	while($row3 = mysqli_fetch_array($queryResult3)){	
		$applianceID3 = explode(',', $row3["applianceID"]);
		$state3  = explode(',', $row3["state"]);
		$power3  = explode(',', $row3["power"]);
		$policy3 = explode(',', $row3["policy"]);
		$name3   = explode(',', $row3["name"]);
		$user3	= explode(',', $row3["user"]);
		$LMP3 = $row3["LMP"];	
	//Peter: Added an if statement to get data for user 3
		for($i = 0; $i < count($applianceID3); $i++){
			if ($user3[$i] == "3") {	
				$tuple3 = array("datestr" => $row3["date"], 
							"applianceID" => $applianceID3[$i],	
							"LMP" => $LMP3,
							"name" => $name3[$i],
							"state" => $state3[$i],
							"power" => $power3[$i],
							"policy" => $policy3[$i]	
							);
											
				echo json_encode($tuple3).",";
			}
		}	
	}
	echo "];";
//	mysql_free_result($queryResult3);
	
	$queryResult4 = mysqli_query($conn, $selectQuery);
	
	$state4 = array();
	$power4 = array();
	$applianceID4 = array();
	
	echo "energyData4 = [";
	// Output the price information to a script
	while($row4 = mysqli_fetch_array($queryResult4)){	
		$applianceID4 = explode(',', $row4["applianceID"]);
		$state4  = explode(',', $row4["state"]);
		$power4  = explode(',', $row4["power"]);
		$policy4 = explode(',', $row4["policy"]);
		$name4   = explode(',', $row4["name"]);
		$user4	= explode(',', $row4["user"]);
		$LMP4 = $row4["LMP"];	
	//Peter: Added an if statement to get data for user 4
		for($i = 0; $i < count($applianceID4); $i++){
			if ($user4[$i] == "4") {	
				$tuple4 = array("datestr" => $row4["date"], 
							"applianceID" => $applianceID4[$i],	
							"LMP" => $LMP4,
							"name" => $name4[$i],
							"state" => $state4[$i],
							"power" => $power4[$i],
							"policy" => $policy4[$i]	
							);
											
				echo json_encode($tuple4).",";
			}
		}	
	}
	echo "];";	
	
//	mysql_free_result($queryResult4);

	mysqli_close($conn);

?>
