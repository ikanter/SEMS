from __future__ import print_function
import csv
import urllib2
import mysql.connector
import re
from mysql.connector import errorcode



#Set database connection
connection = mysql.connector.connect(user    = 'jhamil45',
									password = 'Freeman17',
									host     = 'dbs.eecs.utk.edu',
									database = 'jhamil45')
cursor = connection.cursor()






#Create dictionary of tables
TABLES = {}

#Create Energy table to hold LMP Info
TABLES['Energy'] = '''
	CREATE TABLE Energy(
		Date						CHAR(13) NOT NULL,
		LMP							CHAR(10) DEFAULT 0,
		CongestionPrice				CHAR(10) DEFAULT 0,
		MarginalLossPrice			CHAR(10) DEFAULT 0,
			
		ForecastLMP					CHAR(10) DEFAULT 0,
		ForecastCongestionPrice		CHAR(10) DEFAULT 0,
		ForecastMarginalLossPrice	CHAR(10) DEFAULT 0
		
	) ENGINE=InnoDB'''


#Create Fuel Table to hold Fuel Type Info
TABLES['Fuel'] = '''
	CREATE TABLE Fuel(
		Coal				float(6) DEFAULT 0,
		NaturalGas			float(6) DEFAULT 0,
		Wind				float(6) DEFAULT 0,
		LandFillGas			float(6) DEFAULT 0,
		Solar				float(6) DEFAULT 0,
		MunicipalWaste		float(6) DEFAULT 0,
		Waste				float(6) DEFAULT 0,
		Misc				float(6) DEFAULT 0,
		LightOil			float(6) DEFAULT 0,
		HeavyOil			float(6) DEFAULT 0,
		Diesel				float(6) DEFAULT 0,
		Kerosene			float(6) DEFAULT 0
	)ENGINE=InnoDB'''






#Insert LMP data into the Energy Table
add_LMP = '''
	INSERT INTO Energy
		(LMP, CongestionPrice, MarginalLossPrice, Date)
		VALUES (%s, %s, %s, %s)'''

#Insert Forecasted LMP data into the Energy Table
add_DA = '''
	UPDATE Energy
		SET ForecastLMP = %s,
			ForecastCongestionPrice = %s,
			ForecastMarginalLossPrice = %s
		WHERE Date = %s'''


#Drop Energy Table
drop_energy = '''
	DROP TABLE IF EXISTS Energy'''

#Drop Fuel Table
drop_fuel = '''
	DROP TABLE IF EXISTS Fuel'''







#Drop tables
cursor.execute(drop_energy)
cursor.execute(drop_fuel)

#Iterate through all tables and execute
for name, tab in TABLES.iteritems():
	try:
		#Create Tables
		print("Creating table {}: ".format(name), end='')	
		cursor.execute(tab)
	
	#Print Errors
	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
			print("already exists.")
		else:
			print(err.msg)
	else:
		print("OK")







def databaseify(csvReader, flag):
	#Loop through all rows of CSV file looking for 'Location'
	for row in csvReader:
		if row[2] == LOCATION:
			day = row[0];	#Set day

			for j in range(0,24):	
				if(j != 23):
					dayHour = day + " " + str(j+1) #Add the hour to the day
				else:
					dayHour = day + " 0" #Make midnight(24) 0 
				
				#Insert LMP info
				data = (str(row[(j*3)+7]),	#Index of totalLMP 
						str(row[(j*3)+8]),	#Index of CongestionPrice
						str(row[(j*3)+9]),	#Index of MarginalLoss
						str(dayHour))		#Date Hour

				if(flag == 'LMP'):
					#Insert LMP data into Table
					cursor.execute(add_LMP, data)
					connection.commit()	#Force commit to ensure data goes through

				elif(flag == 'DA'):			
					#Insert Forecasted LMP data into Table
					cursor.execute(add_DA, data)
					connection.commit()	#Force commit to ensure data goes through
						







#Set location to grab the information for
#LOCATION = "CHICKAHOMINY"
LOCATION = "PJM"

#Get the csv file from url and parse LMP file
url = 'http://www.pjm.com/pub/account/lmpmonthly/201602-rt.csv'
response = urllib2.urlopen(url)
crLMP = csv.reader(response)

databaseify(crLMP, 'LMP')



#Get the csv file from url and parse Day After file
url = 'http://www.pjm.com/pub/account/lmpmonthly/201602-da.csv'
response = urllib2.urlopen(url)
crDA = csv.reader(response)

databaseify(crDA, 'DA')








#Dictionary to look up months
months = {
	'JAN': '01',
	'FEB': '02',
	'MAR': '03',
	'APR': '04',
	'MAY': '05',
	'JUN': '06',
	'JUL': '07',
	'AUG': '08',
	'SEP': '09',
	'OCT': '10',
	'NOV': '11',
	'DEC': '12'
}
	

#Get the csv file from url and parse Marginal Fuel file
url = 'http://www.monitoringanalytics.com/data/marginal_fuel_type/201601_Marginal_Fuel_Postings.csv'
response = urllib2.urlopen(url)
crFuel = csv.reader(response)

def databaseifyFuel(add_Table, csvReader):
	for row in csvReader:
		date = row[0]
		day = date[0:2]
		mon = date[2:5]
		mon = months.get(mon)
		year = date[5:9]
		hour = date[10:12]
	#	print(year+str(mon)+day, hour)	


databaseifyFuel(add_DA, crFuel)




#Close SQL Connectors
cursor.close()
connection.close()
