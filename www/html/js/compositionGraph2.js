function PieGraph2(){
	this.outerPadding = 50;
	this.innerPadding = 75;
	this.innerArcRadius = this.innerPadding;
	this.legendPadding = 30;
	this.graph = d3.select("#usage-composition-graph2");
	
	this.colors = ["#81ae6c", "#feba31", "#9dc08c", "#feaa01", "#679451", "#aac99c", "#fecc67"];

	this.init = function(){
		this.initScale();
		this.calcDimensions();
		this.initLayout();
		this.initArcs();
		this.initLegend();
		this.initHoverLegend();
	}
	
	// Calculate the dimensions of the plot and the width of the arcs
	this.calcDimensions = function(){
		this.width = document.getElementById("usage-composition-container2").clientWidth;
		this.height = document.getElementById("usage-composition-container2").clientHeight;
		this.radius = ((d3.min([this.width, this.height]) - this.outerPadding) / 2);
		if(this.radius > 130){
			this.radius = 130;
		}
		this.outerArcRadius = this.radius;
		this.innerArcRadius = this.innerPadding;
	}
	
	this.initScale = function(){
		this.color = d3.scale.ordinal()
			.range(this.colors);
	}

	this.initLayout = function(){
		this.pie = d3.layout.pie()
			.sort(null)
			.value(function(d){
				return d.consumed;
			});
	}

	this.initArcs = function(){
		// Define the arcs for drawing the chart
		this.arc = d3.svg.arc()
			.outerRadius(this.outerArcRadius - 5)
			.innerRadius(this.innerArcRadius);
		this.focusArc = d3.svg.arc()
			.outerRadius(this.outerArcRadius + 5)
			.innerRadius(this.innerArcRadius);
		
		// Add the arc group for the pie chart
		var g = this.graph.append("svg:g")
			.attr("class", "arc-group")
			.attr("transform", "translate(" + (this.radius + (this.outerPadding/2)) + "," + (this.radius + (this.outerPadding/2)) + ")");
		
		// Create the arcs from the data
		var path = g.selectAll("path")
			.data(this.pie(energyUsageTotals2))
			.enter()
			.append("path")
				.attr("class", "arc")
				.attr("d", this.arc)
				.attr("fill", function(d, i){
					return pieGraph.color(d.data.id);
				});
	}

	this.initLegend = function(){
		var g = this.graph.append("svg:g")
			.attr("class", "pie-legend")
			.attr("transform", "translate(" + 300 + ",150)");
		
		// Pick the top 8 appliances by their energy usage, since we don't have space to display all appliances
		var sortedByTop = energyUsageTotals2.slice(0);
		sortedByTop.sort(function(d1, d2){
			return d2.consumed - d1.consumed;
		});	
		var numLegendItems = sortedByTop.length < 8 ? sortedByTop.length : 8;
		
		// Center the legend
		var centerY = this.radius + (this.outerPadding/2);
		var half = numLegendItems/2;
		var g2;
		for(var i = 0; i < numLegendItems; i++){	
			if(numLegendItems % 2 == 0){
				if(half - i > 0){
					g2 = g.append("svg:g")
						.attr("transform", "translate(" + this.legendPadding + "," + (-15 - ((half - i - 1) * 30)) + ")");
				}
				else{
					g2 = g.append("svg:g")
						.attr("transform", "translate(" + this.legendPadding + "," + (15 + ((i - half) * 30)) + ")");
				}
			}
			else{
				if(Math.floor(half) - i > 0){
					g2 = g.append("svg:g")
						.attr("transform", "translate(" + this.legendPadding + "," + (-30 - ((Math.floor(half) - i - 1) * 30)) + ")");
				}
				else if(Math.floor(half) - i == 0){
					g2 = g.append("svg:g")
						.attr("transform", "translate(" + this.legendPadding + ",0)");
				}
				else{
					g2 = g.append("svg:g")
						.attr("transform", "translate(" + this.legendPadding + "," + (((i - Math.floor(half)) * 30)) + ")");
				}
			}
			
			// Calculate the total energy usage and each appliance's percentage to display
			var total = d3.sum(energyUsageTotals2.map(function(d){
				return d.consumed;
			}));
			var percent = 100 * (sortedByTop[i].consumed / total);
			
			// Add the legend rectangle and text
			g2.append("svg:rect")
				.attr("width", 20)
				.attr("height", 20)
				.attr("shape-rendering", "crispEdges")
				.attr("fill", this.color(sortedByTop[i].id));
			g2.append("svg:text")
				.attr("x", 30)
				.attr("y", 14)
				.text(sortedByTop[i].name + ": " + percent.toFixed(2) + "%");
			}
	}
	
	// Initialize the legend that shows as the user hovers over arcs
	this.initHoverLegend = function(){
		
		// Create the text
		var g = this.graph.append("svg:g")
			.attr("class", "pie-hover")	
			.attr("transform", "translate(" + (this.radius + (this.outerPadding/2)) + "," + (this.radius + (this.outerPadding/2)) + ")");
		var text = g.append("svg:text")
			.attr("x", 0)
			.attr("y", 0);
		text.append("svg:tspan")
			.attr("x", 0)
			.attr("y", -15)
			.attr("text-anchor", "middle")	
			.attr("id", "pie-hover-appliance");
		text.append("svg:tspan")
			.attr("x", 0)
			.attr("y", 5)
			.attr("text-anchor", "middle")
			.attr("id", "pie-hover-energy");
		text.append("svg:tspan")
			.attr("x", 0)
			.attr("y", 25)
			.attr("text-anchor", "middle")
			.attr("id", "pie-hover-percent");
		
		// Have each arc listen for mouseover events
		this.graph.selectAll(".arc")
			.on("mouseover", function(d){
				var total = d3.sum(energyUsageTotals2.map(function(d){
					return d.consumed;
				}));
				var percent = 100 * (d.data.consumed / total);
				
				// Draw any larger arcs back to their normal size
				pieGraph.graph.selectAll(".arc")
					.transition().duration(200)
					.attr("d", pieGraph.arc)
					.attr("class", "arc");
				
				// Update the legend in the center of the chart
				pieGraph.graph.select(".pie-hover")
					.attr("opacity", 1);
				pieGraph.graph.select("#pie-hover-appliance")
					.text(d.data.name);
				pieGraph.graph.select("#pie-hover-energy")
					.text((d.data.consumed / 1000).toFixed(2) + " kWh");
				pieGraph.graph.select("#pie-hover-percent")
					.text(percent.toFixed(2) + "%");
				
				// Draw the arc larger as it is hovered
				d3.select(this)
					.transition().duration(200)
					.attr("class", "arc focused")
					.attr("d", pieGraph.focusArc);	
			});	
	}
	
	// Resizes as the window gets resized
	this.resize = function(){
		// Recalculate the dimensions of the plot
		this.calcDimensions();
		if(this.width <= 0){
			return;
		}	
	
		// Resize and position the arcs
		this.arc
			.outerRadius(this.outerArcRadius - 5)
			.innerRadius(this.innerArcRadius);
		this.focusArc
			.outerRadius(this.outerArcRadius + 5)
			.innerRadius(this.innerArcRadius);
		this.graph.select(".arc-group")
			.attr("transform", "translate(" + (this.radius + (this.outerPadding/2)) + "," + (this.radius + (this.outerPadding/2)) + ")");
		this.graph.selectAll(".arc")
			.attr("d", this.arc);
		this.graph.selectAll(".arc.focused")
			.attr("d", this.focusArc);
		
		// Reposition the hover tooltip
		this.graph.select(".pie-hover")
			.attr("transform", "translate(" + (this.radius + (this.outerPadding/2)) + "," + (this.radius + (this.outerPadding/2)) + ")");
	}
}
