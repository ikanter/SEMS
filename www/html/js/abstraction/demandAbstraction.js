function drawDemandChart(){
  var nyData = [];
  var chart;
  $(document).ready(function() {
  	$.extend({
  	getUrlVars: function(){
  		var vars = [], hash;
  		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  		for(var i = 0; i < hashes.length; i++)     {
  			hash = hashes[i].split('=');
  			vars.push(hash[0]);
  			vars[hash[0]] = hash[1];
  		}
  		return vars;
  		},
  		getUrlVar: function(name){
  			return $.getUrlVars()[name];
  		}
  	});

  	var zone = '';
  	var counter = 0;
  	var FINAL_COUNT;
  	var globalDate = new Date();
  	var rnd;
  	rnd = globalDate.getFullYear() + '' + globalDate.getMonth() + '' + globalDate.getDate() + '' + globalDate.getHours() + '' + (Math.round(globalDate.getMinutes()/5)*5);

  	var totalLoad = [];
  	var yestTotalLoad = [];
  	var totalBID = [];
  	var yestTotalBID = [];
  	var tomTotalBID = [];

  	var zoneObj = new Object;
  		zoneObj['ZoneA']={zone:'a',name:'Zone A - West',ptid:'61752', yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col11', yestBid:[], bid:[], tomBid:[],color:'#0066cc', visible:false};
  		zoneObj['ZoneB']={zone:'b',name:'Zone B - Genesee',ptid:'61753',yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col4',yestBid:[], bid:[], tomBid:[],color:'#6600cc', visible:false};
  		zoneObj['ZoneC']={zone:'c',name:'Zone C - Central',ptid:'61754',yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col2',yestBid:[], bid:[], tomBid:[],color:'#cc00cc', visible:false};
  		zoneObj['ZoneD']={zone:'d',name:'Zone D - North',ptid:'61755',yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col10',yestBid:[], bid:[], tomBid:[],color:'#00cccc', visible:false};
  		zoneObj['ZoneE']={zone:'e',name:'Zone E - Mohawk Valley',ptid:'61756',yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col7',yestBid:[], bid:[], tomBid:[],color:'#47a3ff', visible:false};
  		zoneObj['ZoneF']={zone:'f',name:'Zone F - Capital',ptid:'61757',yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col1',yestBid:[], bid:[], tomBid:[],color:'#cc0066', visible:false};
  		zoneObj['ZoneG']={zone:'g',name:'Zone G - Hudson Valley',ptid:'61758',yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col5',yestBid:[], bid:[], tomBid:[],color:'#ff850a', visible:false};
  		zoneObj['ZoneH']={zone:'h',name:'Zone H - Millwood',ptid:'61759', yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col8',yestBid:[], bid:[], tomBid:[],color:'#cc6600', visible:false};
  		zoneObj['ZoneI']={zone:'i',name:'Zone I - Dunwoodie',ptid:'61760',yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col3',yestBid:[], bid:[], tomBid:[],color:'#cccc00', visible:false};
  		zoneObj['ZoneJ']={zone:'j',name:'Zone J - New York City',ptid:'61761',yestLoad:[], load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col9',yestBid:[], bid:[], tomBid:[],color:'#cc0000', visible:true};
  		zoneObj['ZoneK']={zone:'k',name:'Zone K - Long Island',ptid:'61762',yestLoad:[],load:[],forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col6',yestBid:[], bid:[], tomBid:[],color:'#00cc00', visible:false};
  		zoneObj['ZoneT']={zone:'total',name:' NYS Total Load', yestLoad:yestTotalLoad, load:totalLoad, forecast:[], tomForecast:[], yestForecast:[], forecastCol: 'col12', yestBid:yestTotalBID, bid:totalBID, tomBid:tomTotalBID,color:'#000099', visible:false};

  	if($.getUrlVar('zone')){
  	 	zone = $.getUrlVar('zone');
  	 	var thisZone = 'Zone' + zone.toUpperCase();
  	 	zoneObj[thisZone].visible = true;
  	}
  	var d = new Date();
  	var timeStamp = d.getMonth() + '' + d.getDate() + '' + d.getFullYear() + '' + d.getHours() + '' + d.getMinutes() + '' + d.getSeconds();
  	var globalDate = d;
  	var tomorrowDate = new Date();
  	tomorrowDate.setDate(globalDate.getDate() + 1);
  	var yesterdayDate = new Date();
  	yesterdayDate.setDate(globalDate.getDate() - 1);
  	getData();

  function getDateFormatted(date){
  	var thisMonth = date.getMonth()+1;
  	if (thisMonth<10){
  		thisMonth = '0' +  thisMonth;
  	}
  	var dateFormatted = date.getFullYear() + '' + thisMonth;
  	if (date.getDate()<10){
  		dateFormatted = dateFormatted + '0' + date.getDate();
  	}else{
  		dateFormatted = dateFormatted + '' + date.getDate();
  	}
  	return dateFormatted;
  }

  function dateToUTC(date){
  	var thisMonth = date.getMonth();
  	var thisDay = date.getDate();
  	var thisYear = date.getFullYear();
  	var thisHour = date.getHours();
  	var thisMin = date.getMinutes();
  	return Date.UTC(thisYear, thisMonth, thisDay, thisHour, thisMin);
  }

  function getData(){
  	$("#loading").show();

  	var dateFormatted = getDateFormatted(globalDate);
  	var yesterdateFormatted = getDateFormatted(yesterdayDate);
  	var tomdateFormatted = getDateFormatted(tomorrowDate);

  	var yestloadMIS = "http://mis.nyiso.com/public/csv/pal/" + yesterdateFormatted + "pal.csv";
  	var yestloadURL = "select * from csv where url = '" + yestloadMIS + "'";
  	var loadMIS = "http://mis.nyiso.com/public/csv/pal/" + dateFormatted + "pal.csv";
  	var loadURL = "select * from csv where url = '" + loadMIS + "'";

  	var forecastMIS = "http://mis.nyiso.com/public/csv/isolf/" + yesterdateFormatted + "isolf.csv";
  	var forecastURL = "select * from csv where url = '" + forecastMIS + "'";

  	var yestbidMIS = "http://mis.nyiso.com/public/csv/zonalBidLoad/" + yesterdateFormatted + "zonalBidLoad.csv";
  	var yestbidURL = "select * from csv where url = '" + yestbidMIS + "'";
  	var bidMIS = "http://mis.nyiso.com/public/csv/zonalBidLoad/" + dateFormatted + "zonalBidLoad.csv";
  	var bidURL = "select * from csv where url = '" + bidMIS + "'";
  	var tombidMIS = "http://mis.nyiso.com/public/csv/zonalBidLoad/" + tomdateFormatted + "zonalBidLoad.csv";
  	var tombidURL = "select * from csv where url = '" + tombidMIS + "'";

  	FINAL_COUNT = 6;
  	getLoadData(loadURL);
  	getLoadData(yestloadURL);
  	getForecastData(forecastURL);
  	getBidData(yestbidURL);
  	getBidData(bidURL);
  	getBidData(tombidURL);
  }

  function setLoadData(data){
  	if(data.query.count > 0){
  		$.each(data.query.results.row, function(i, v) {
  			$.each(zoneObj, function(k,z) {
  				if (v.col3 == z.ptid) {
  					if(parseFloat(v.col4)){
  						var thisData = [];
  						var dateStr = v.col0;
  						var thisDate = new Date(dateStr);
  						var dateUTC = dateToUTC(thisDate);
  						thisData.push(dateUTC);
  						thisData.push(parseFloat(v.col4));
  						var totals = [];
  						if(k === 'ZoneA'){
  							totals.push(dateUTC);
  							totals.push(0);
  						}
  						if(thisDate.getDate() == globalDate.getDate()){
  							zoneObj[k].load.push(thisData);
  							//Add the Time Intervals to the Array of Totals, with Load = 0 for each time
  							if(k === 'ZoneA'){
  								totalLoad.push(totals);
  							}
  						}else if(thisDate.getDate() == yesterdayDate.getDate()){
  							zoneObj[k].yestLoad.push(thisData);
  							//Add the Time Intervals to the Array of Totals, with Load = 0 for each time
  							if(k === 'ZoneA'){
  								yestTotalLoad.push(totals);
  							}
  						}
  					}
  				 }
  			 });
  		});
  	}
  	drawChart();
  }

  function getLoadData(url){
  	$.ajax({
  		url: 'http://query.yahooapis.com/v1/public/yql',
  		data: {
  			q: url,
  			format: 'json',
  			_maxage: 120,
  			rnd: rnd
  		},
  		cache:true,
  		dataType: 'jsonp',
  		success:function(data,status,rsp){
      		setLoadData(data);
    		}
  	});
  }

  function setForecastData(data){
  	if(data.query.count > 0){
  		$.each(data.query.results.row, function(i, v) {
  			if(i != 0){
  				var dateStr = v.col0;
  				var thisDate = new Date(dateStr);
  				var dateUTC = dateToUTC(thisDate);
  				if(thisDate.getDate() == globalDate.getDate()){
  					$.each(zoneObj, function(k,z) {
  						var thisData = [];
  						var col = v[zoneObj[k].forecastCol];
  						thisData.push(dateUTC);
  						thisData.push(parseFloat(col));
  						zoneObj[k].forecast.push(thisData);
              //console.log(zoneObj[k]);
  					});
  				}
  				if(thisDate.getDate() == tomorrowDate.getDate()){
  					$.each(zoneObj, function(k,z) {
  						var thisData = [];
  						var col = v[zoneObj[k].forecastCol];
  						thisData.push(dateUTC);
  						thisData.push(parseFloat(col));
  						zoneObj[k].tomForecast.push(thisData);
  					});
  				}
  				if(thisDate.getDate() == yesterdayDate.getDate()){
  					$.each(zoneObj, function(k,z) {
  						var thisData = [];
  						var col = v[zoneObj[k].forecastCol];
  						thisData.push(dateUTC);
  						thisData.push(parseFloat(col));
  						zoneObj[k].yestForecast.push(thisData);
  					});
  				}
  			}
  		});
  	}
  	drawChart();
  }

  function getForecastData(url){
  	$.ajax({
  		url: 'http://query.yahooapis.com/v1/public/yql',
  		data: {
  			q: url,
  			format: 'json',
  			_maxage: 120,
  			rnd: rnd
  		},
  		cache:true,
  		dataType: 'jsonp',
  		success:function(data,status,rsp){
      		setForecastData(data);
    		}
  	});
  }

  function setBidData(data){
  	if(data.query.count > 0){
  		$.each(data.query.results.row, function(i, v) {
  			$.each(zoneObj, function(k,z) {
  			if (v.col3 == z.ptid) {
  			var thisData = [];
  				var dateStr = v.col0;
  	    		var thisDate = new Date(dateStr);
  				var dateUTC = dateToUTC(thisDate);
  				thisData.push(dateUTC);
  				thisData.push(parseFloat(v.col4));
  				var totals = [];
  				if(k === 'ZoneA'){
  					totals.push(dateUTC);
  					totals.push(0);
  				}
  				if(thisDate.getDate() == globalDate.getDate()){
  					zoneObj[k].bid.push(thisData);
  					if(k === 'ZoneA'){
  						totalBID.push(totals);
  					}
  				}else if(thisDate.getDate() == yesterdayDate.getDate()){
  					zoneObj[k].yestBid.push(thisData);
  					if(k === 'ZoneA'){
  						yestTotalBID.push(totals);
  					}
  				}else if(thisDate.getDate() == tomorrowDate.getDate()){
  					zoneObj[k].tomBid.push(thisData);
  					if(k === 'ZoneA'){
  						tomTotalBID.push(totals);
  					}
  				}
  			 }
  			 });
  		});
  	}
  	drawChart();
  }

  function getBidData(url){
  	$.ajax({
  		url: 'http://query.yahooapis.com/v1/public/yql',
  		data: {
  			q: url,
  			format: 'json',
  			_maxage: 120,
  			rnd: rnd
  		},
  		cache:true,
  		dataType: 'jsonp',
  		success:function(data,status,rsp){
      		setBidData(data);
    		}
  	});
  }

  function drawChart(){
    counter = counter + 1;
    if (counter == FINAL_COUNT){

    var dMth = globalDate.getMonth() + 1;
    var dDay = globalDate.getDate();
    var dYr = globalDate.getFullYear();
    var dStr = dMth + '/' + dDay + '/' + dYr;

    title = dStr + ' - Zonal Load';


    Highcharts.theme = { colors: ['#4572A7'] };// prevent errors in default theme

    var seriesData= [];
    $.each(zoneObj, function(k,z) {
    	var visi;
    	var actualData = [];
    	var forecastData = [];
    	var bidsData = [];
    	if (z.zone == zone){
    		visi = true;
    	}else{
    		visi = false;
    	}

    	actualData.type = 'line';
    	actualData.id = z.zone;
    	actualData.name = z.name;
    	actualData.data = zoneObj[k].load;
    	actualData.visible = visi;
    	actualData.color = z.color;
    	actualData.step = true;
    	actualData.events = {
    		legendItemClick: function(event) {
    			var loadId = this.options.id + 'load';
    			if (this.visible == true){
    				var series = chart.get(loadId);
    				series.hide();
    			}else{
    				var series = chart.get(loadId);
    				series.show();
    			}
    		},
    		click: function(event) {
    			this.hide();
    		}
    	};
    	if(k != 'ZoneT'){
    		for(var i=0; i< zoneObj[k].load.length; i++) {
    			totalLoad[i][1] += parseFloat(zoneObj[k].load[i][1]);
    		}
    		for(var i=0; i< zoneObj[k].yestLoad.length; i++) {
    			yestTotalLoad[i][1] += parseFloat(zoneObj[k].yestLoad[i][1]);
    		}
    		for(var i=0; i< zoneObj[k].bid.length; i++) {
    			totalBID[i][1] += parseFloat(zoneObj[k].bid[i][1]);
    		}
    		for(var i=0; i< zoneObj[k].yestBid.length; i++) {
    			yestTotalBID[i][1] += parseFloat(zoneObj[k].yestBid[i][1]);
    		}
    		for(var i=0; i< zoneObj[k].tomBid.length; i++) {
    			tomTotalBID[i][1] += parseFloat(zoneObj[k].tomBid[i][1]);
    		}
    	}
    	seriesData.push(actualData);

    	forecastData.type = 'line';
    	forecastData.id = z.zone+'forecast';
    	forecastData.name = z.name + ' Forecast';
    	forecastData.data = zoneObj[k].forecast;
    	forecastData.visible = visi;
    	forecastData.color = z.color;
    	forecastData.dashStyle = 'ShortDash';
    	forecastData.showInLegend = false;
    	forecastData.events = {
    		click: function(event) {
    			this.hide();
    		}
    	};
    	seriesData.push(forecastData);

    	bidsData.type = 'line';
    	bidsData.id = z.zone+'bids';
    	bidsData.name = z.name + ' DAM Bids';
    	bidsData.data = zoneObj[k].bid;
    	bidsData.visible = visi;
    	bidsData.color = z.color;
    	bidsData.dashStyle = 'ShortDot';
    	bidsData.showInLegend = false;
    	bidsData.events = {
    		click: function(event) {
    			this.hide();
    		}
    	};
    	seriesData.push(bidsData);
    });

       chart = new Highcharts.Chart({
          chart: {
             renderTo: 'chartContainer',
             zoomType: 'xy'

          },
           title: {
             text: title
          },
           subtitle: {
             text: document.ontouchstart === undefined ?
                'Click and drag in the plot area to zoom in' :
                'Drag your finger over the plot to zoom in'
          },
          xAxis: [{
    			 type: 'datetime'
    		  },{
    			type: 'datetime',
    			linkedTo: 0,
    			opposite: false,
    			tickInterval: 24 * 3600 * 1000,
    			labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%m/%d/%Y', this.value);
                    }
                }
    		  }
    	  ],
    	  yAxis: {
             title: {
                text: 'Megawatts (MW)'
             },
    		 startOnTick: true,
             showFirstLabel: true
          },
          tooltip: {
             crosshairs: true,
             borderColor: '#909090',
             formatter: function() {
             	var s = '<b>' + Highcharts.dateFormat('%m/%d/%Y %H:%M', this.x) +'</b><br/>';
                $.each(this.points, function(i, p) {
                	s = s + '<span style="color:' + p.point.series.color + '">' + p.series.name + '</span> : ' + Highcharts.numberFormat(p.y, 2) + ' MW<br/>';
                });
                return s;

             },
             shared:true
          },
          legend: {
          	 enabled: false,
             layout: 'vertical',
             align: 'right',
             verticalAlign: 'top',
             x: -10,
             y: 50,
             borderWidth: 0
          },
          plotOptions: {
             line: {
                lineWidth: 2,
                marker: {
                   enabled: false,
                   states: {
                      hover: {
                         enabled: true,
                         radius: 4
                      }
                   }
                },
                states: {
                	hover: {
                    	enabled: true,
                        lineWidth: 2
                    }
                },
                shadow: false
             }
          },
          series: seriesData
       });
       $("#loading").hide();
       paintChart();
    }
  }

  $.each(zoneObj, function(k,z) {
  	var $legend = $('#legend');
  	var linkClass = (z.visible == true) ? '' : 'hidden';

  	$('<li id="legend' + z.zone + '">')
  		.css('color', z.color)
  		.text(z.name)
  		.click(function(){
      		toggleSeries(z.zone);
  		})
  		.addClass(linkClass)
          .hover(function () {
          	$(this).addClass("highlight");
           },
           function () {
           	$(this).removeClass("highlight");
           })
  		.appendTo($legend);
  });

  function toggleSeries(zone){
  	$.each(zoneObj, function(k,z) {
  		var legendId = '#legend' + z.zone;
  		if(z.zone == zone){
  			if (z.visible == true){
  				z.visible = false;
  				$(legendId).addClass('hidden');
  			}else{
  				z.visible = true;
  				$(legendId).removeClass('hidden');
  			}
  			return;
  		}
  	});
  	paintChart();
  }

  function paintChart(){
  	var actualEnabled = $('#showActual').is(':checked');
  	var forecastEnabled = $('#showForecast').is(':checked');
  	var bidsEnabled = $('#showBids').is(':checked');
  	var tomorrowEnabled = $('#showTomorrow').is(':checked');
  	var yesterdayEnabled = $('#showYesterday').is(':checked');
  	$.each(chart.series, function(i,s) {
  		thisZone = 'Zone' + s.name.charAt(5);
  		if (s.name.indexOf('Forecast')!=-1){
  			if(zoneObj[thisZone].visible==true){
  				var allForecastData = zoneObj[thisZone].forecast;
  				s.setData(allForecastData);
  				if (forecastEnabled ==true){
  					if (s.visible == false){
  						s.show();
  					}
  				} else {
  					if (s.visible == true){
  						s.hide();
  					}
  				}
  			}else{
  				if (s.visible == true){
  					s.hide();
  				}
  			}
  		}else if (s.name.indexOf('Bid')!=-1){
  			if(zoneObj[thisZone].visible==true){
  				var allBidData = zoneObj[thisZone].bid;
  				if(yesterdayEnabled){
  					allBidData = allBidData.concat(zoneObj[thisZone].yestBid);
  				}
  				s.setData(allBidData);
  				if (bidsEnabled ==true){
  					if (s.visible == false){
  						s.show();
  					}
  				} else {
  					if (s.visible == true){
  						s.hide();
  					}
  				}
  			}else{
  				if (s.visible == true){
  					s.hide();
  				}
  			}
  		}else{
  			var allLoadData = zoneObj[thisZone].load;
  			if(yesterdayEnabled){
  				allLoadData = allLoadData.concat(zoneObj[thisZone].yestLoad);
  			}
  			s.setData(allLoadData);
  			if(zoneObj[thisZone].visible==true){
  				if (actualEnabled ==true){
  					if (s.visible == false){
  						s.show();
  					}
  				}else{
  					if (s.visible == true){
  						s.hide();
  					}
  				}
  			}else{
  				if (s.visible == true){
  					s.hide();
  				}
  			}
  		}
  	});
  }

  $("#showActual").click(function() {
  	paintChart();
  });
  $("#showForecast").click(function() {
  	paintChart();
  });
  $("#showTomorrow").click(function() {
  	paintChart();
  });
  $("#showYesterday").click(function() {
  	paintChart();
  });
  });
}
