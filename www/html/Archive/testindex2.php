<?php

define("DB_SERVER", "localhost");
define("DB_USER", "root");
define("DB_PASS", "root");
define("DB_NAME", "test");

$db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

if(mysqli_connect_errno()) {
	$msg = "Database connection failed: ";
	$msg .= mysqli_connect_error();
	$msg .= " (" . mysqli_connect_errno() . ")";
	exit($msg);
}

$sql = "SELECT * FROM LMP ";

//$sql .= "WHERE dateID < 1250 ";

$sql .= "ORDER BY dateID ASC";
//echo $sql;
$result_set = mysqli_query($db, $sql);

if (!$result_set) {
	exit("Database query failed.");
}

?>
  
  <!DOCTYPE html>
  <html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="js/newscripts.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
		var data = google.visualization.arrayToDataTable([
		  [<?php echo "'Time'" ?>, <?php echo "'Frequency'" ?>],
		  <?php while($result = mysqli_fetch_assoc($result_set)) { 
					if(is_numeric($result['LMP']) && $result['LMP'] > 0 && $result['LMP'] == round($result['LMP'], 0))
						$freq = (60 - 0.2 * log($result['LMP'])) + 0.6;
					else if($result['LMP'] == 0)
						$freq = 60;
					else if($result['LMP'] < 0)
						$freq = 60;
					if (!is_numeric($freq))
						$freq = 60;

		  ?>
		  ['<?php echo $result['Date']; ?>', <?php echo $freq; ?>],
		  <?php } ?>
		  ['16:49:00',  60]
		]);

		var options = {
		  title: 'Frequency',
		  curveType: 'function',
		  legend: { position: 'bottom' }
		};

		var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

		chart.draw(data, options);
	}
      
    </script>
	
  </head>
  <body>
	
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
	
  </body>
</html>
