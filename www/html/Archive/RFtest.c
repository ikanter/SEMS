#include<stdio.h>
#include<wiringPi.h>
#define short_delay 160
#define long_delay 520
#define extended_delay 5620
#define numberAttempt 10
#define transmitPin 4
char a_on[]="1111101010101010110011001";
char a_off[]="1111101010101010110000111";
char b_on[]="1111101010101010001011001";
char b_off[]="1111101010101010001000111";
char c_on[]="1111101010101000111011001";
char c_off[]="1111101010101000111000111";
char d_on[]="1111101010100010111011001";
char d_off[]="1111101010100010111000111";
main (void){
 wiringPiSetup();
 pinMode(transmitPin,OUTPUT);
 int j;
 for (j=0;j<numberAttempt;++j){
  
 int i;
 for (i=0;i<25;++i){
  if (a_off[i]=='1'){
   digitalWrite(transmitPin, HIGH);
   delayMicroseconds(short_delay);
   digitalWrite(transmitPin, LOW);
   delayMicroseconds(long_delay);
}
  else if (a_off[i]=='0'){
   digitalWrite(transmitPin, HIGH);
   delayMicroseconds(long_delay);
   digitalWrite(transmitPin, LOW);
   delayMicroseconds(short_delay);

}
}
 digitalWrite(transmitPin, LOW);
 delayMicroseconds(extended_delay);

}
return 0; 
}
