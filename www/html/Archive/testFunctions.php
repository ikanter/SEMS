<?phpfunction getIn(){

  

    //Normally defined in a function
    define("DB_SERVER", "localhost");
    define("DB_USER", "root");
    define("DB_PASS", "root");
    define("DB_NAME", "test");

    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

    if(mysqli_connect_errno()) {
    //Self-Explanatory
    $msg = "Database connection failed: ";
    $msg .= mysqli_connect_error();
    $msg .= " (" . mysqli_connect_errno() . ")";
    exit($msg);
    }
    //The Query
    //$sql = "SELECT id FROM deviceID WHERE deviceID = '" . $deviceID . "'";
    $sql = "SELECT * FROM userSettings";
    //Pass in the connection
    $result = mysqli_query($connection, $sql);

    if (!$result) {
    	exit("Database query failed.");
    }

    // while($threshold = mysqli_fetch_assoc($result)) { ?>
       <script>
    //
    //     var threshold = [];
    //     threshold.push(<?php echo $threshold['threshold']; ?>);
    //     console.log(threshold);
    //     console.log(threshold.length);
       </script>
    <?php //}

     if(isset($connection)) {
		mysqli_close($connection);
     }
 ?>

  <script>
    function getIt(){
      $(document).ready(function() {
        $.extend({
        getUrlVars: function(){
          var vars = [], hash;
          var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
          for(var i = 0; i < hashes.length; i++)     {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
          }
          return vars;
          },
          getUrlVar: function(name){
            return $.getUrlVars()[name];
          }
        });
		<?php //echo "console.log('Here');";?>
        var zone = '';
        var counter = 0;
        var FINAL_COUNT;
        var globalDate = new Date();
        var rnd;
        rnd = globalDate.getFullYear() + '' + globalDate.getMonth() + '' + globalDate.getDate() + '' + globalDate.getHours() + '' + (Math.round(globalDate.getMinutes()/5)*5);


        var zoneObj = new Object;
          zoneObj['ZoneA']={zone:'a',name:'Zone A - West',ptid:'61752', yestData:[], data:[], yestLoad:[], load:[],color:'#0066cc', visible:false};
          zoneObj['ZoneB']={zone:'b',name:'Zone B - Genesee',ptid:'61753', yestData:[], data:[], yestLoad:[],load:[],color:'#6600cc', visible:false};
          zoneObj['ZoneC']={zone:'c',name:'Zone C - Central',ptid:'61754', yestData:[], data:[], yestLoad:[],load:[],color:'#cc00cc', visible:false};
          zoneObj['ZoneD']={zone:'d',name:'Zone D - North',ptid:'61755',yestData:[], data:[], yestLoad:[],load:[],color:'#00cccc', visible:false};
          zoneObj['ZoneE']={zone:'e',name:'Zone E - Mohawk Valley',ptid:'61756', yestData:[],data:[],yestLoad:[],load:[],color:'#47a3ff', visible:false};
          zoneObj['ZoneF']={zone:'f',name:'Zone F - Capital',ptid:'61757',yestData:[],data:[], yestLoad:[],load:[],color:'#cc0066', visible:false};
          zoneObj['ZoneG']={zone:'g',name:'Zone G - Hudson Valley',ptid:'61758',yestData:[],data:[], yestLoad:[],load:[],color:'#ff850a', visible:false};
          zoneObj['ZoneH']={zone:'h',name:'Zone H - Millwood',ptid:'61759',yestData:[],data:[],yestLoad:[],load:[],color:'#cc6600', visible:false};
          zoneObj['ZoneI']={zone:'i',name:'Zone I - Dunwoodie',ptid:'61760',yestData:[],data:[],yestLoad:[],load:[],color:'#cccc00', visible:false};
          zoneObj['ZoneJ']={zone:'j',name:'Zone J - New York City',ptid:'61761',yestData:[],data:[],yestLoad:[],load:[],color:'#cc0000', visible:false};
          zoneObj['ZoneK']={zone:'k',name:'Zone K - Long Island',ptid:'61762',yestData:[],data:[],yestLoad:[],load:[],color:'#00cc00', visible:false};

        if($.getUrlVar('zone')){
          zone = $.getUrlVar('zone');
          var thisZone = 'Zone' + zone.toUpperCase();
          zoneObj[thisZone].visible = true;
        }

        var globalDate = new Date();
        var tomorrowDate = new Date();
        tomorrowDate.setDate(globalDate.getDate() + 1);
        var yesterdayDate = new Date();
        yesterdayDate.setDate(globalDate.getDate() - 1);
        var timeStamp = globalDate.getMonth() + '' + globalDate.getDate() + '' + globalDate.getFullYear() + '' + globalDate.getHours() + '' + globalDate.getMinutes() + '' + globalDate.getSeconds();
        getData();

      var tempNum = 0;

      function getDateFormatted(date){
        var thisMonth = date.getMonth()+1;
        if (thisMonth<10){
          thisMonth = '0' +  thisMonth;
        }
        var dateFormatted = date.getFullYear() + '' + thisMonth;
        if (date.getDate()<10){
          dateFormatted = dateFormatted + '0' + date.getDate();
        }else{
          dateFormatted = dateFormatted + '' + date.getDate();
        }
        return dateFormatted;
      }

      function getData(){

        var dateFormatted = getDateFormatted(globalDate);
        // var yesterdateFormatted = getDateFormatted(yesterdayDate);
        // var tomdateFormatted = getDateFormatted(tomorrowDate);

        // var yestNYMIS = "http://mis.nyiso.com/public/csv/realtime/" + yesterdateFormatted + "realtime_zone.csv";
        // var yestNYUrl = "select * from csv where url = '" + yestNYMIS + "'";
        var nyMIS = "http://mis.nyiso.com/public/csv/realtime/" + dateFormatted + "realtime_zone.csv";
        var nyUrl = "select * from csv where url = '" + nyMIS + "'";

        // var yestLoadMIS = "http://mis.nyiso.com/public/csv/pal/" + yesterdateFormatted + "pal.csv";
        // var yestLoadUrl = "select * from csv where url = '" + yestLoadMIS + "'";
        // var loadMIS = "http://mis.nyiso.com/public/csv/pal/" + dateFormatted + "pal.csv";
        // var loadUrl = "select * from csv where url = '" + loadMIS + "'";

        // FINAL_COUNT = 4;
        getNYData(nyUrl);

        // getNYData(yestNYUrl);
        // getLoadData(loadUrl);
        // getLoadData(yestLoadUrl);
        // console.log(nyUrl);
      }

      function getNYData(url){
        $.ajax({
          url: 'http://query.yahooapis.com/v1/public/yql',
          data: {
            q: url,
            format: 'json',
            _maxage: 120,
            rnd: rnd
          },
          cache:true,
          dataType: 'jsonp',
          success:function(data,status,rsp){
              setNYData(data);
              // console.log("Success");
            }
        });
      }

      function setNYData(data){
        var prices = [];
        // console.log(data.query.results.row[0]);
        // console.log("2: " + data.query.results.row[1].col1 + ":  " + data.query.results.row[1].col3);
        // console.log("3: " + data.query.results.row[2].col1 + ":  " + data.query.results.row[2].col3);
        // console.log("4: " + data.query.results.row[3].col1 + ":  " + data.query.results.row[3].col3);
        // console.log("5: " + data.query.results.row[4].col1 + ":  " + data.query.results.row[4].col3);
        // console.log("6: " + data.query.results.row[5].col1 + ":  " + data.query.results.row[5].col3);
        // console.log("7: " + data.query.results.row[6].col1 + ":  " + data.query.results.row[6].col3);
        // console.log("8: " + data.query.results.row[7].col1 + ":  " + data.query.results.row[7].col3);
        // console.log("9: " + data.query.results.row[8].col1 + ":  " + data.query.results.row[8].col3);
        // console.log("10: " + data.query.results.row[9].col1 + ":  " + data.query.results.row[9].col3);
        // console.log("11: " + data.query.results.row[10].col1 + ":  " + data.query.results.row[10].col3);
        // console.log("12: " + data.query.results.row[11].col1 + ":  " + data.query.results.row[11].col3);
        // console.log("13: " + data.query.results.row[12].col1 + ":  " + data.query.results.row[12].col3);
        // console.log("14: " + data.query.results.row[13].col1 + ":  " + data.query.results.row[13].col3);

        if(data.query.count > 0){
          $.each(data.query.results.row, function(i, v) {
            $.each(zoneObj, function(k,z) {
            if (v.col2 == z.ptid) {
              var thisData = [];
              var dateStr = v.col0;
              var thisDate = new Date(dateStr);
              // var dateUTC = dateToUTC(thisDate);
              // console.log("Global: " + globalDate.getDate());
              // console.log("This:   " + thisDate.getDate());

              if (data.query.results.row[i].col1 == "N.Y.C." && thisDate.getFullYear() == globalDate.getFullYear() && thisDate.getMonth() == globalDate.getMonth() && thisDate.getDate() == globalDate.getDate() && thisDate.getHours() == globalDate.getHours() && globalDate.getMinutes() <= thisDate.getMinutes()){
                tempNum += 1;
                console.log(tempNum + ": " + data.query.results.row[i].col0 + ": " + data.query.results.row[i].col1 + ":  " + data.query.results.row[i].col3);
                prices.push(Number(data.query.results.row[i].col3));
              }

              // thisData.push(dateUTC);
              thisData.push(parseFloat(v.col3));

              if(thisDate.getDate() == globalDate.getDate()){
                if(thisDate <= globalDate){
                  zoneObj[k].data.push(thisData);
                  // console.log(thisDate);
                }
              }else if(thisDate.getDate() == yesterdayDate.getDate()){
                zoneObj[k].yestData.push(thisData);
              }
             }
             });
          });
        }
        // drawChart();
        var threshold = [];
        var policy = [];
        var state = [];
        var id = [];
        <?php
			define("DB_SERVER", "localhost");
			define("DB_USER", "root");
			define("DB_PASS", "root");
			define("DB_NAME", "test");

			$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

			if(mysqli_connect_errno()) {
			//Self-Explanatory
			$msg = "Database connection failed: ";
			$msg .= mysqli_connect_error();
			$msg .= " (" . mysqli_connect_errno() . ")";
			exit($msg);
			}
			//The Query
			//$sql = "SELECT id FROM deviceID WHERE deviceID = '" . $deviceID . "'";
			$sql = "SELECT * FROM userSettings";
			//Pass in the connection
			$result = mysqli_query($connection, $sql);

			if (!$result) {
				exit("Database query failed.");
			}
			while($threshold = mysqli_fetch_assoc($result)) { ?>
				//console.log(<?php echo $threshold['onOffPolicy']; ?>);
				threshold.push(<?php echo $threshold['threshold']; ?>);
				policy.push(<?php echo $threshold['onOffPolicy']; ?>);
				state.push(<?php echo $threshold['state']; ?>);
				id.push(<?php echo $threshold['applianceID']; ?>);
		<?php
				//echo "console.log(threshold); ";
				//echo "console.log(threshold.length); ";
			} 
			mysqli_free_result($result);
		    if(isset($connection)) {
				mysqli_close($connection);
			}
		?>
        console.log("globalDate: " + globalDate);
        var latestPrice = prices.pop();
        var testingBool = false;
        var testingPrice = 0.5;
        console.log("Final Array Value Before if: " + latestPrice);
        if (testingPrice){
			console.log("Final Array Value In if: " + testingPrice);
			for (i = 0; i < threshold.length; i++){
				if (policy[i] == 1){
					if (testingPrice > threshold[i]){//testingPrice > threshold[i]
						if (i == 0){
							console.log(0);
							changeState(id[i]);
						}
						else if (i == 1){
							console.log(1);
							changeState(id[i]);
						}
						else if (i == 2){
							console.log(2);
							changeState(id[i]);
						}
						else if (i == 3){
							console.log(3);
							changeState(id[i]);
						}
						else if (i == 4){
							console.log(4);
							changeState(id[i]);
						}
						else if (i == 5){
							console.log(5);
							changeState(id[i]);
						}
						else if (i == 6){
							console.log(6);
							changeState(id[i]);
						}
						else if (i == 7){
							console.log(7);
							changeState(id[i]);
						} else{
							console.log("You shouldn't be here");
							}
					} else{
						console.log("This ID made it to the else testing threshold: " + id[i]);
					  }
				} else{
					console.log("This ID made it to the else testing policy: " + id[i]);
				  }
			}
        }
      }
      });
    }
    
    function changeState(id){
		
		$.ajax({

			 url : 'action/changeState.php',
			 type : 'POST',
			 data : id,
			 success : function (result) {
				console.log (result); // Here, you need to use response by PHP file.
			 },
			 error : function () {
				console.log ('error');
			 }

	   });
   }

    setInterval(getIt, 5000);

  </script>
<?php} ?>
