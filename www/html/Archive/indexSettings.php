<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>SHEMS Web App</title>	
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>
			
		<script type="text/javascript" src="js/compositionGraph.js"></script>
		<script type="text/javascript" src="js/energyConsumptionGraph.js"></script>
		<script type="text/javascript" src="js/priceGraph.js"></script>
		<script type="text/javascript" src="js/frequencyGraph.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/index.css">
	</head>
		<body id="body">
		<header class="container-fluid" id="header">
			<div class="container">
				<a href="#" onClick="return showHome()">
					<h2 id="brand">SHEMS<br>
					<span id="brand-small">Smart Home Energy Management System</span></h2>
				</a>
			</div>
		</header>

		<header class="navbar-inverse" id="navbar">
			<div class="container">	
				<ul class="nav navbar-nav">
					<li id="navHome" class="nav-link active"><a href="#" onclick="return showHome()">Home</a></li>
					<li id="navSettings" class="nav-link"><a href="#" onclick="return showSettings()">User Settings</a></li>	
					<li id="navPricing" class="nav-link"><a href="#" onclick="return showPricing()">Price History</a></li>
					<li id="navFrequency1" class="nav-link"><a href="testindex.php">Frequency History 1</a></li>	
					<li id="navFrequency2" class="nav-link"><a href="testindex2.php">Frequency History 2</a></li>	
				</ul>
			</div>
		</header>
				<section id="settings" class="page shown">
					<div class="section-title">
						<h4>Appliance Settings</h4>
					</div>

					<div id="time-menu-container">
						<div id="timeMenu">
							<h4>Add/Edit Scheduled Off Periods</h4>	
							<hr></hr>
							<div id="timeMenuForm" class="container">
								<div>
									<label for="time-begin-h" class="time-label">Start Time: </label>
									<input id="time-begin-h" type="text" value="12" class="time-input form-control" maxlength="2"><label for="time-begin-m">:</label>
									<input id="time-begin-m" type="text" value="00" class="time-input form-control" maxlength="2">
								</div>
								<div class="col-xs-12"></div>
							
								<div>
									<label for="time-end-h" class="time-label">End Time: </label>
									<input id="time-end-h" type="text" value="12" class="time-input form-control" maxlength="2"><label for="time-end-m">:</label>
									<input id="time-end-m" type="text" value="00" class="time-input form-control" maxlength="2">
								</div>
								<br><br>
								<div id="days-container">
									<a id="day-sun" href="#" class="time-select-day" onclick="return toggleDay('day-sun')">S</a>
									<a id="day-mon" href="#" class="time-select-day" onclick="return toggleDay('day-mon')">M</a>
									<a id="day-tue" href="#" class="time-select-day" onclick="return toggleDay('day-tue')">T</a>
									<a id="day-wed" href="#" class="time-select-day" onclick="return toggleDay('day-wed')">W</a>
									<a id="day-thu" href="#" class="time-select-day" onclick="return toggleDay('day-thu')">T</a>
									<a id="day-fri" href="#" class="time-select-day" onclick="return toggleDay('day-fri')">F</a>
									<a id="day-sat" href="#" class="time-select-day" onclick="return toggleDay('day-sat')">S</a>
								</div>
								<input id="day-sun-hidden" type="hidden" value="0"></input>
								<input id="day-mon-hidden" type="hidden" value="0"></input>
								<input id="day-tue-hidden" type="hidden" value="0"></input>
								<input id="day-wed-hidden" type="hidden" value="0"></input>
								<input id="day-thu-hidden" type="hidden" value="0"></input>
								<input id="day-fri-hidden" type="hidden" value="0"></input>
								<input id="day-sat-hidden" type="hidden" value="0"></input>
								<br><br>
								<button id="time-submit" onclick="return saveOffPeriod()">Save</button><button id="time-cancel" onclick="return hideTimeMenu()">Cancel</button><button id="time-delete" onclick="return hideTimeMenu()">Delete</button>
							</div>
						</div>
					</div>

					<div id="confirm-delete-container">
						<div id="confirm-delete-dialog" class="container">
							<h4>Delete Appliance Settings</h4>
							<hr></hr>
							<p>Are you sure you want to delete <strong id="confirm-delete-name"></strong>? This action cannot be undone.</p>
							<button id="confirm-delete-button">Yes</button><button onclick="return hideDeleteDialog()">No</button>
						</div>
					</div>

					<div class="content-section-container">
						<ul id="appliance-list" class="content-section">
							Loading...
						</ul>

						<button id="new-appliance-button" onclick="addAppliance()">Add a New Appliance</button>
					</div>
				</section>
			</section>
		</body>	
</html>
