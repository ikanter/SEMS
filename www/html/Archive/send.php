<?php

	define("DB_SERVER", "localhost");
	define("DB_USER", "root");
	define("DB_PASS", "root");
	define("DB_NAME", "test");
	
    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    
    if(mysqli_connect_errno()) {
      $msg = "Database connection failed: ";
      $msg .= mysqli_connect_error();
      $msg .= " (" . mysqli_connect_errno() . ")";
      exit($msg);
    }
    
    $sql = "UPDATE userSettings SET ";
    $sql .= "onOffPolicy=0";

    $result = mysqli_query($connection, $sql);
    // For UPDATE statements, $result is true/false
    if($result) {
      return true;
    } else {
      // UPDATE failed
      echo mysqli_error($connection);
      db_disconnect($connection);
      exit;
    }
    
    if(isset($connection)) {
      mysqli_close($connection);
    }
?>
